package com.example.vacation_manager.Home;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoLatestProperty;
import com.example.vacation_manager.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

@SuppressWarnings("all")
class AdapterHomeProperties extends RecyclerView.Adapter<AdapterHomeProperties.MyHolder> {
    private Context context;
    private List<PojoLatestProperty.Property> pojoPropertyList;
    private final PropertyClickListener propertyClickListener;
     AdapterHomeProperties(List<PojoLatestProperty.Property> pojoProperty, Context context, PropertyClickListener propertyClickListener) {
        this.propertyClickListener = propertyClickListener;
        this.context = context;
        this.pojoPropertyList = pojoProperty;
    }

    @NonNull
    @Override
    public AdapterHomeProperties.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_properties, parent, false);
        return new MyHolder(view,propertyClickListener );
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHomeProperties.MyHolder holder, int position) {
        PojoLatestProperty.Property pojo = pojoPropertyList.get(position);
        if(pojo.getName()!=null)
        holder.textViewName.setText(pojo.getName());
        Transformation tr = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(5)
                .oval(false)
                .build();
        if(pojo.getImage()!=null)
        Picasso.with(context).load("http://vm.appening.xyz/".concat(pojo.getImage())).fit().transform(tr).into(holder.image);
        if(pojo.getCity()!=null)
        holder.textViewCity.setText(pojo.getCity());
        if(pojo.getNoOfBeds()!=null)
        holder.textViewNoOfBeds.setText("" + pojo.getNoOfBeds());
        if(pojo.getBathrooms()!=null)
        holder.textViewBathrooms.setText("" + pojo.getBathrooms());
        if(pojo.getPricePerNight()!=null)
        holder.textViewPricePerNight.setText("" + pojo.getPricePerNight());
        if(pojo.getRatingCount()!=null)
        holder.textViewAverageRating.setText("("+pojo.getRatingCount()+")");
        if(pojo.getAverageRating()==null){
            holder.ratingBar.setRating(0);
        }else {
            holder.ratingBar.setRating(Float.parseFloat(String.valueOf(pojo.getAverageRating())));
        }
    }

    @Override
    public int getItemCount() {
        return pojoPropertyList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        RelativeLayout row_Apartment;
        ImageView image;
        TextView textViewName, textViewCity, textViewNoOfBeds, textViewBathrooms, textViewPricePerNight, textViewAverageRating;
        RatingBar ratingBar;
        PropertyClickListener propertyClickListener;
        MyHolder(@NonNull View itemView, final PropertyClickListener propertyClickListener) {
            super(itemView);
            row_Apartment = itemView.findViewById(R.id.layoutRowApart);
            textViewName = itemView.findViewById(R.id.textViewPropertyName);
            image = itemView.findViewById(R.id.imgViewProperties);
            textViewCity = itemView.findViewById(R.id.textViewCity);
            textViewNoOfBeds = itemView.findViewById(R.id.textViewNoOfBed);
            textViewBathrooms = itemView.findViewById(R.id.txtViewNoOfWashroom);
            textViewPricePerNight = itemView.findViewById(R.id.txViewtPrice);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            textViewAverageRating= itemView.findViewById(R.id.averageRating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(propertyClickListener!=null){
                        int position =getAdapterPosition();
                        final PojoLatestProperty.Property property= pojoPropertyList.get(position);
                        propertyClickListener.onPropertyItemClick(property.getId());
                    }
                }
            });
        }
    }
    public interface PropertyClickListener{
        void onPropertyItemClick(int position);
    }
}
