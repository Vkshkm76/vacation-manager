package com.example.vacation_manager.Home;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoCity;
import com.example.vacation_manager.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class AdapterCity extends RecyclerView.Adapter<AdapterCity.MyViewHolder> {

    private Context context;
    public List<PojoCity.FeatureCity> cityList;
    private final OnItemClickListener itemClickListener;

    AdapterCity(List<PojoCity.FeatureCity> city, Context context, OnItemClickListener itemClickListener) {
        this.cityList = city;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_city, parent, false);
        return new MyViewHolder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final PojoCity.FeatureCity city = cityList.get(position);
        holder.name.setText(city.getCity());
        Transformation tr = new RoundedTransformationBuilder().borderColor(Color.WHITE)
                .borderWidthDp(1).cornerRadiusDp(5).oval(false).build();
        Picasso.with(context).load("http://vm.appening.xyz/" + city.getCityImage()).fit().transform(tr).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        TextView name;
        ImageView img;
        OnItemClickListener itemClickListener;

        MyViewHolder(@NonNull View itemView, final OnItemClickListener itemClickListener) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.rowCity);
            name = itemView.findViewById(R.id.txtName);
            img = itemView.findViewById(R.id.imgVw);
            this.itemClickListener = itemClickListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        int position = getAdapterPosition();
                        final PojoCity.FeatureCity city = cityList.get(position);
                        itemClickListener.onItemClick(city.getId());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}