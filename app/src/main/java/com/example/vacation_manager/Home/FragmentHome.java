package com.example.vacation_manager.Home;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import androidx.appcompat.widget.SearchView;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Api;
import com.example.vacation_manager.Pojo.PojoCity;
import com.example.vacation_manager.Pojo.PojoLatestProperty;
import com.example.vacation_manager.Pojo.PojoSearchlocationList;
import com.example.vacation_manager.R;
import com.example.vacation_manager.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ALL")
public class FragmentHome extends Fragment implements AdapterCity.OnItemClickListener, AdapterHomeProperties.PropertyClickListener {
    private RecyclerView recyclerCity, recyclerLatestProperties;
    final String TAG = "FragmentHome";
    private View view;
    private AdapterCity adapterCity;
    private RelativeLayout rowCity;
    Communication communication;
    Toolbar toolbar;
    private SearchView.OnQueryTextListener queryTextListener;
    AutoCompleteTextView mSearchAutoComplete;

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        getActivity().setActionBar(toolbar);
        getCityDataApi();
        getPropertyData();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communication = (Communication) context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectSearchApi();
    }

    private void connectSearchApi() {
        final Api api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoSearchlocationList> call = api.getLocationList("origin_input");
        call.enqueue(new Callback<PojoSearchlocationList>() {
            @Override
            public void onResponse(Call<PojoSearchlocationList> call, Response<PojoSearchlocationList> response) {
                if (response.isSuccessful()) {
                    setSearchView(response.body().getList());
                    Log.d(TAG, "onResponse: Search");
                }
            }

            @Override
            public void onFailure(Call<PojoSearchlocationList> call, Throwable t) {
            }
        });
    }

    private void setSearchView(final List<String> list) {
        mSearchAutoComplete = view.findViewById(R.id.searchView);
        final ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, list);
        listAdapter.notifyDataSetChanged();
        listAdapter.setNotifyOnChange(true);

        mSearchAutoComplete.setAdapter(listAdapter);
        mSearchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String queryString = (String) adapterView.getItemAtPosition(position);
                communication.searchItem(queryString);
                mSearchAutoComplete.showDropDown();
            }
        });
    }

    private void propertyData(List<PojoLatestProperty.Property> properties) {
        recyclerLatestProperties = view.findViewById(R.id.recyclerApartment);
        LinearLayoutManager llm2 = new LinearLayoutManager(getContext());
        llm2.setOrientation(RecyclerView.VERTICAL);
        recyclerLatestProperties.setLayoutManager(llm2);
        AdapterHomeProperties adapter = new AdapterHomeProperties(properties, getContext(), this);
        adapter.notifyDataSetChanged();
        recyclerLatestProperties.setAdapter(adapter);
    }

    private void cityData(List<PojoCity.FeatureCity> featureCityList) {
        recyclerCity = view.findViewById(R.id.recyclerCity);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerCity.setLayoutManager(llm);
        AdapterCity adapter = new AdapterCity(featureCityList, getContext(), this);
        adapter.notifyDataSetChanged();
        recyclerCity.setAdapter(adapter);
    }

    private void getCityDataApi() {
        final Api apiCity = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoCity> call = apiCity.getCity();
        call.enqueue(new Callback<PojoCity>() {
            @Override
            public void onResponse(Call<PojoCity> call, Response<PojoCity> response) {
                if (response.body() != null) {
                    cityData(response.body().getFeatureCity());
                    Log.d(TAG, "onResponse: City Data");
                }
            }

            @Override
            public void onFailure(Call<PojoCity> call, Throwable t) {
            }
        });
    }

    private void getPropertyData() {
        final Api api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoLatestProperty> listcall = api.getLatestProperty();
        listcall.enqueue(new Callback<PojoLatestProperty>() {
            @Override
            public void onResponse(@Nullable Call<PojoLatestProperty> call, @Nullable Response<PojoLatestProperty> response) {
                if (response.body() != null) {
                    propertyData(response.body().getProperties());
                    Log.d(TAG, "onResponse: Property Data");
                }
            }

            @Override
            public void onFailure(@Nullable Call<PojoLatestProperty> call, @Nullable Throwable t) {
            }
        });
    }

    @Override
    public void onItemClick(int id) {
        communication.initFragmentProperty(id);
    }

    @Override
    public void onPropertyItemClick(int id) {
        communication.initFragmentDetail(id);
    }

    public interface Communication {
        void initFragmentProperty(int id);
        void initFragmentDetail(int id);
        void searchItem(String item);
    }
}