package com.example.vacation_manager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.example.vacation_manager.Booking.FragmentAvailability;
import com.example.vacation_manager.Details.FragmentDetailPage;
import com.example.vacation_manager.Details.PropertyActivity;
import com.example.vacation_manager.Home.FragmentHome;
import com.example.vacation_manager.Map.FragmentMap;
import com.example.vacation_manager.Properties.FragmentProperties;

@SuppressWarnings("all")
public class MainActivity extends AppCompatActivity implements FragmentHome.Communication, FragmentProperties.PropertiesFragmentListner
                    , FragmentMap.CommunicationMap{
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    final String TAG="MainActivity";
    FragmentAvailability fragmentAvailability;
    Bundle bundle;
    private static final String PROPERTY_JSON_KEY="PROPERTY_JSON";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bundle=new Bundle();
        loadFragment(new FragmentHome());
    }

    private void loadFragment(FragmentHome fragmentHome) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutMainActivity, fragmentHome).addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void setSecondFragment(FragmentProperties fragment, int id) {
        bundle.putInt("id", id);
        fragment=new FragmentProperties();
        fragment.setArguments(bundle);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutMainActivity, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    @Override
    public void initFragmentProperty(int id) {
        setSecondFragment(new FragmentProperties(),id);
    }

    @Override
    public void initFragmentDetail(int id) {
        setFragmentPropertyDetail(new FragmentDetailPage(), id);
    }

    @Override
    public void searchItem(String item) {
        bundle.putString("item", item);

        FragmentProperties fragment =new FragmentProperties();
        fragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutMainActivity, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setFragmentPropertyDetail(FragmentDetailPage fragmentDetailPage, int id) {
        Intent intent = new Intent(MainActivity.this, PropertyActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    @Override
    public void onMapClicked(String pojoProperties) {
        bundle.putString(PROPERTY_JSON_KEY, pojoProperties);
        FragmentMap fragment=new FragmentMap();
        fragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutMainActivity, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}