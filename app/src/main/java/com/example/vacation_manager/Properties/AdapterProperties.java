package com.example.vacation_manager.Properties;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoProperties;
import com.example.vacation_manager.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;
@SuppressWarnings("all")
class AdapterProperties extends RecyclerView.Adapter<AdapterProperties.FeaturedHolder> {
    private Context context;
    private List<PojoProperties.Featured> pojoFeaturedList;
    private PropertyClickListener propertyClickListener;
    AdapterProperties(List<PojoProperties.Featured> featured, Context context, PropertyClickListener propertyClickListener) {
        this.context = context;
        this.pojoFeaturedList = featured;
        this.propertyClickListener = propertyClickListener;
    }

    @NonNull
    @Override
    public AdapterProperties.FeaturedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_property, parent, false);
        return new FeaturedHolder(view, propertyClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterProperties.FeaturedHolder holder, int position) {
        final PojoProperties.Featured pojo = pojoFeaturedList.get(position);
        holder.textViewName.setText(pojo.getName());

        Transformation tr = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE).borderWidthDp(1).cornerRadiusDp(5).oval(false).build();
        Picasso.with(context).load("http://vm.appening.xyz/".concat(pojo.getImage())).fit().transform(tr).into(holder.image);
        holder.textViewCity.setText(pojo.getCity());
        holder.textViewNoOfBeds.setText("" + pojo.getNoOfBeds());
        holder.textViewBathrooms.setText("" + pojo.getBathrooms());
        holder.textViewPricePerNight.setText("" + pojo.getPricePerNight());
        holder.textViewAverageRating.setText("("+pojo.getRatingCount()+")");
        if(pojo.getAverageRating()==null){
            holder.ratingBar.setRating(0);
        }else {
            holder.ratingBar.setRating(Float.parseFloat(String.valueOf(pojo.getAverageRating())));
        }
                if(pojo.getIsFavourite()==0)
                holder.favouriteButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                else
                    holder.favouriteButton.setImageResource(R.drawable.ic_favorite_red_24dp);

                holder.favouriteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(pojo.getIsFavourite()==0) {
                            pojo.setIsFavourite(1);
                            holder.favouriteButton.setImageResource(R.drawable.ic_favorite_red_24dp);
                        }else{
                            pojo.setIsFavourite(0);
                            holder.favouriteButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                        }
                    }
                });
    }

    @Override
    public int getItemCount() {
        return pojoFeaturedList.size();
    }

    class FeaturedHolder extends RecyclerView.ViewHolder {
        RelativeLayout row_Apartment;
        ImageView image,favouriteButton;
        TextView textViewName, textViewCity, textViewNoOfBeds, textViewBathrooms, textViewPricePerNight, textViewAverageRating;
        RatingBar ratingBar;
        PropertyClickListener propertyClickListener;
        FeaturedHolder(@NonNull final View itemView, final PropertyClickListener propertyClickListener) {
            super(itemView);
            row_Apartment = itemView.findViewById(R.id.layoutRowApart);
            textViewName = itemView.findViewById(R.id.textViewPropertyName);
            image = itemView.findViewById(R.id.imgViewProperties);
            textViewCity = itemView.findViewById(R.id.textViewCity);
            textViewNoOfBeds = itemView.findViewById(R.id.textViewNoOfBed);
            textViewBathrooms = itemView.findViewById(R.id.txtViewNoOfWashroom);
            textViewPricePerNight = itemView.findViewById(R.id.txViewtPrice);
            ratingBar=itemView.findViewById(R.id.ratingBar);
            textViewAverageRating= itemView.findViewById(R.id.averageRating);
            favouriteButton = itemView.findViewById(R.id.favouriteToggleButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final PojoProperties.Featured property = pojoFeaturedList.get(position);

                    propertyClickListener.itemClick(property.getId());
                }
            });
        }
    }
    public interface PropertyClickListener{
        void itemClick(int id);
    }
}
