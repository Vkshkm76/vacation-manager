package com.example.vacation_manager.Properties;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import com.example.vacation_manager.Api;
import com.example.vacation_manager.FragmentSavedListing;
import com.example.vacation_manager.Pojo.PojoProperties;
import com.example.vacation_manager.R;
import com.example.vacation_manager.RetrofitInstance;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ALL")
public class FragmentProperties extends Fragment implements AdapterProperties.PropertyClickListener {
    private final static String TAG = "FragmentProperties";
    private View view;
    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerFeatured;
    PropertiesFragmentListner propertiesFragmentListner;
    int id;
//    String listItem;
    String propertyJson;
    ImageView backButton;
    Toolbar toolbar;
    Button buttonSavedList;
    ImageView favouriteButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        propertiesFragmentListner = (PropertiesFragmentListner) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_properties, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        favouriteButton = view.findViewById(R.id.favouriteToggleButton);
        ((AppCompatActivity) getContext()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getContext()).setTitle("");
        backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        floatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                propertiesFragmentListner.onMapClicked(propertyJson);
            }
        });
        buttonSavedList = view.findViewById(R.id.buttonSaved);
        buttonSavedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutMainActivity, new FragmentSavedListing()).commit();
            }
        });
        id = getArguments().getInt("id");
        getPropertyApi(id);
        return view;
    }

    @Override
    public void itemClick(int id) {
        propertiesFragmentListner.initFragmentDetail(id);
    }

    private void featuredData(List<PojoProperties.Featured> featured, List<PojoProperties.Featured> nonFeatureds) {
        recyclerFeatured = view.findViewById(R.id.recycFrag2Property);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerFeatured.setLayoutManager(llm);
        featured.addAll(nonFeatureds);

        AdapterProperties adapter = new AdapterProperties(featured, getContext(), this);
        adapter.notifyDataSetChanged();
        recyclerFeatured.setNestedScrollingEnabled(false);
        recyclerFeatured.setAdapter(adapter);
    }

    private static Gson gson;
    private static Gson getGsonParser() {
        if (null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }

    private void getPropertyApi(int id) {
        String authorization = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFkZmU0ZTk5MzQ2NGE3YmUzYjI1YjQ1YjcxZDFiYTg5MmQ2NGU2NmZlMWMwY2Y4NWIyNGEyN2M5MjEwMGNjOWY2NzliYmNjNDQ3ZjAzNjY4In0.eyJhdWQiOiIyIiwianRpIjoiYWRmZTRlOTkzNDY0YTdiZTNiMjViNDViNzFkMWJhODkyZDY0ZTY2ZmUxYzBjZjg1YjI0YTI3YzkyMTAwY2M5ZjY3OWJiY2M0NDdmMDM2NjgiLCJpYXQiOjE1NjQ3NDkwMjksIm5iZiI6MTU2NDc0OTAyOSwiZXhwIjoxNTY3NDI3NDg5LCJzdWIiOiI0NiIsInNjb3BlcyI6WyIqIl19.S0qUdagXpNLWYFRKZiB7AQ9Hj1zVolxAWBdeQYF4JVIm31qbvTkH-VQtrWN588siPD4rOKa_Qs1tpMM_RUEsY9LB4t5Es5MNyx7wpdMD7jdY6pe6Y_spVxvonMNwDycO2RBsZrDQhT3YED6o5PsA1FCTlIejEwvpICnwN9VOIPQkuOAmQxpdnWuR7ahTaYPUpJwYXVCipHBn7gNAKNpRqbLifLmVbg_37f13akq1p-xhAqJrGFVZyELaUoA2qBBlkRLtKXDHPzGKY0FL0zFK5MlnyWdVjXPaPU4V-jo54brI3NGbxWHp8ImJTBpyUqWVfGgQ7jLbAz_YCEkHZ5rBftbqg3fUXetFxQQGBuSzG4_l2H1f5KAhwF04WHNYNPAiEHG_ldwg4UHb6v0lrPN8g3MgL8OZTp1rk_mEDvMAh7IzKUGWP_ldZsytsMStUz7YGDXd4tAvLSA1kFdVUaf2ht2RJS3oCigCFOH5Ka74S-UuvSnBHziyMUFqD_-ZupAkKDGYI5V2su8v3IonMI5BtZCvW68ApYJFyeOPKMH3MGwf-U42Uy1OLZnr58XeraEVBfB242iE1RcQFg1HGpVk0-ceWmMuSB5CLSjk_rPx_08Iuh7ay7Wrphy2vu2v1nmRtcpaNh88k8ZzHvj9YmdEX-7bSrjQmSq0_pGke3aM_TM";
        final Api api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoProperties> pojo_featuredCall = api.savePost(id, authorization);

        pojo_featuredCall.enqueue(new Callback<PojoProperties>() {
            @Override
            public void onResponse(Call<PojoProperties> call, Response<PojoProperties> response) {
                if (response.body() != null) {
                    propertyJson = getGsonParser().toJson(response.body());
                    featuredData(response.body().getFeatured(), response.body().getNonFeatured());
                }
            }

            @Override
            public void onFailure(Call<PojoProperties> call, Throwable t) {
            }
        });
    }
    public interface PropertiesFragmentListner {
        void onMapClicked(String propertyJson);
        void initFragmentDetail(int id);
    }
}
