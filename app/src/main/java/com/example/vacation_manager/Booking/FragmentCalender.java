package com.example.vacation_manager.Booking;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vacation_manager.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;

import java.time.format.DateTimeFormatter;
import java.util.List;
@SuppressWarnings("all")
public class FragmentCalender extends Fragment implements OnRangeSelectedListener {
    View view;
    MaterialCalendarView calenderRange;
    final String TAG = "FragmentCalender";
    String checkinDate, checkOutDate;
    CalendarListener calendarListener;
    Button buttonSubmit;
    Context context;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE, d MMM yyyy");

    public FragmentCalender() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calender, container, false);
        calenderRange = view.findViewById(R.id.calendarView);
        buttonSubmit = view.findViewById(R.id.buttonSetCalender);
        calenderRange.setOnRangeSelectedListener(new OnRangeSelectedListener() {
            @Override
            public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull List<CalendarDay> dates) {
                if (dates.size() > 0) {
                    int index1,index2;
                    String d=dates.toString();
                    index1=d.indexOf("{");
                    index2=d.indexOf("}");
                    checkinDate = dates.get(0).toString().substring(index1, index2-1);
                    checkOutDate = (dates.get(dates.size() - 1)).toString().substring(index1, index2-1);
                }
                setDate(checkinDate, checkOutDate);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        calendarListener = (CalendarListener) context;
        super.onAttach(context);
    }

    @Override
    public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull List<CalendarDay> dates) {
    }

    private void setDate(final String checkinDate, final String checkOutDate) {
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarListener.onDateRangeSelected(checkinDate,checkOutDate);
            }
        });
    }

    public interface CalendarListener {
        void onDateRangeSelected(String startDate, String endDate);
        void loadContactHost(String startDate, String endDate);
       // void loadBookingAvailability();
    }
}
