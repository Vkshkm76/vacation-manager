package com.example.vacation_manager.Booking;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.vacation_manager.R;

import java.util.ArrayList;

class CustomAdapterBottomSheet implements ListAdapter {
    ArrayList<PojoBottomSheet> arrayList;
    Context context;
    final String TAG="CustomAdapter";
    public CustomAdapterBottomSheet(Context context, ArrayList<PojoBottomSheet> arrayList) {
        this.arrayList=arrayList;
        this.context = context;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        TextView textViewItem,textViewValue;
        view=LayoutInflater.from(context).inflate(R.layout.row_custom_adapter,null);

        textViewItem= view.findViewById(R.id.textViewItem);
        textViewValue = view.findViewById(R.id.textViewValue);
        Log.d(TAG, "getView: "+arrayList.get(position).subject);
        Log.d(TAG, "getView: fees "+arrayList.get(position).value);
        textViewItem.setText(arrayList.get(position).subject);
        textViewValue.setText(""+arrayList.get(position).value);
        return view;
    }

    @Override
    public int getItemViewType(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        if(arrayList.size()<=1)
            return 1;
        else
        return arrayList.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
