package com.example.vacation_manager.Booking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoEventList;
import com.example.vacation_manager.R;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterEvent extends RecyclerView.Adapter<AdapterEvent.HolderEvent> {
    List<PojoEventList.Datum> eventData;
    Context context;
    View view;
    final String TAG="AdapterEvent";
    public AdapterEvent(List<PojoEventList.Datum> data, Context context) {
    this.eventData = data;
    this.context = context;
    }

    @NonNull
    @Override
    public HolderEvent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view= LayoutInflater.from(context).inflate(R.layout.row_events_booking,parent,false);
        return new HolderEvent(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderEvent holder, int position) {
        if(eventData.get(position).getName()!=null)
        holder.textViewEventName.setText(eventData.get(position).getName());
        if(eventData.get(position).getDate()!=null)
        holder.textViewDate.setText(eventData.get(position).getDate());
        if(eventData.get(position).getAddress()!=null)
        holder.textViewEventLocation.setText(eventData.get(position).getAddress());
        if(eventData.get(position).getImage()!=null)
        Picasso.with(context).load(eventData.get(position).getImage()).into(holder.imageViewEvents);
    }

    @Override
    public int getItemCount() {
        return eventData.size();
    }

    public class HolderEvent extends RecyclerView.ViewHolder {
        ImageView imageViewEvents;
        TextView textViewDate,textViewEventName,textViewEventLocation;
        public HolderEvent(@NonNull View itemView) {
            super(itemView);
            imageViewEvents =itemView.findViewById(R.id.imageViewEvents);
            textViewDate =itemView.findViewById(R.id.textViewDate);
            textViewEventName=itemView.findViewById(R.id.textViewEventName);
            textViewEventLocation =itemView.findViewById(R.id.textViewEventLocation);

        }
    }
}
