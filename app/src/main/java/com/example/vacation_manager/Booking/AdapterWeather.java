package com.example.vacation_manager.Booking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoWeatherList;
import com.example.vacation_manager.R;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterWeather extends RecyclerView.Adapter<AdapterWeather.HolderWeather> {
    List<PojoWeatherList.Datum> weatherData;
    Context context;
    View view;
    public AdapterWeather(List<PojoWeatherList.Datum> data, Context context) {
        this.weatherData=data;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderWeather onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.row_weather_booking,parent,false);
        return new HolderWeather(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderWeather holder, int position) {
        if(weatherData.get(position).getTime()!=null)
        holder.textViewDate.setText(weatherData.get(position).getTime().toString());
        holder.textViewTemperature.setText(weatherData.get(position).getHighTemp().toString()+
                " / "+weatherData.get(position).getLowTemp().toString());
        holder.textViewWeatherStatus.setText(weatherData.get(position).getName());
        Picasso.with(context).load("http://vm.appening.xyz/"+weatherData.get(position).getIcon()).into(holder.imageViewWeather);
    }

    @Override
    public int getItemCount() {
        return weatherData.size();
    }

    public class HolderWeather extends RecyclerView.ViewHolder {
        TextView textViewDate,textViewTemperature,textViewWeatherStatus;
        ImageView imageViewWeather;
        public HolderWeather(@NonNull View itemView) {
            super(itemView);
            textViewDate=itemView.findViewById(R.id.textViewDate);
            imageViewWeather = itemView.findViewById(R.id.imageViewWeather);
            textViewTemperature = itemView.findViewById(R.id.textViewTemperature);
            textViewWeatherStatus=itemView.findViewById(R.id.textViewWeatherStatus);

        }
    }
}
