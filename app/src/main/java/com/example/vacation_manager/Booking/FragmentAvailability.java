package com.example.vacation_manager.Booking;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.vacation_manager.Api;
import com.example.vacation_manager.ContactHost.FragmentContactHost;
import com.example.vacation_manager.Pojo.PojoBookingAvailability;
import com.example.vacation_manager.Pojo.PojoEventList;
import com.example.vacation_manager.Pojo.PojoWeatherList;
import com.example.vacation_manager.R;
import com.example.vacation_manager.RetrofitInstance;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")

public class FragmentAvailability extends Fragment {
    private ImageView backButton;
    private TextView textViewCheckIn, textViewCheckOut, textViewGuests, textViewPrice, textViewTotalNights, textViewCheckPrice,
            textViewEvents, buttonInstantBooking, buttonContactHost;
    private RecyclerView recyclerViewWeather, recyclerViewEvent;
    View view;
    DatePickerDialog datePickerDialog;
    int year;
    int month;
    int dayOfMonth;
    Context context;
    String checkinDate, checkOutDate;
    final String TAG = "FragmentAvailability";
    private Api api;
    BottomSheetBehavior bottomSheetBehavior;
    ListView listView;
    ArrayList<PojoBottomSheet> bookingData = new ArrayList<>();

    public FragmentAvailability() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        connectWeatherApi();
        connectEventApi();
        connectBookingStatusApi();
        view = inflater.inflate(R.layout.fragment_availability, container, false);
        findViews();
        final View viewBottomSheet = view.findViewById(R.id.bottom_sheet);
        listView = view.findViewById(R.id.listViewBottomSheet);

        textViewCheckPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetBehavior = BottomSheetBehavior.from(viewBottomSheet);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View view, @BottomSheetBehavior.State int newState) {
                        if(newState==BottomSheetBehavior.STATE_HIDDEN)
                            Log.d(TAG, "onStateChanged: State hidden");

                    }

                    @Override
                    public void onSlide(@NonNull View view, float v) {
                    }
                });
            }
        });
        return view;
    }

    private void connectBookingStatusApi() {
        String check_in = "12/11/2018", check_out = "15/11/2018";
        int guest = 3;
        api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoBookingAvailability> call = api.getAvailability(2034, check_in, check_out, guest);
        call.enqueue(new Callback<PojoBookingAvailability>() {
            @Override
            public void onResponse(Call<PojoBookingAvailability> call, Response<PojoBookingAvailability> response) {
                if (response.body() != null)
                    setStatusData(response.body().getData());
            }

            @Override
            public void onFailure(Call<PojoBookingAvailability> call, Throwable t) {
            }
        });
    }

    private void setStatusData(PojoBookingAvailability.Data data) {
        TextView textViewPayableAmount,textViewDateRange;
        ImageView imageViewBackButton;
        textViewPayableAmount = view.findViewById(R.id.textViewPayableAmount);
        imageViewBackButton = view.findViewById(R.id.imageViewCrossButton);
        textViewDateRange=view.findViewById(R.id.textViewDateRange);

        if (getArguments() != null)
        textViewDateRange.setText(getArguments().getString("checkInDate")
                +" to "+getArguments().getString("checkOutDate"));
        textViewPayableAmount.setText(data.getPayableAmount().toString());
        bookingData.add(new PojoBottomSheet("Per Night Charge", Double.valueOf(data.getPerNightCharge())));
        bookingData.add(new PojoBottomSheet("Sub Total", data.getSubTotal()));
        bookingData.add(new PojoBottomSheet("Tax", data.getTax()));
        bookingData.add(new PojoBottomSheet("Stripe Fee", data.getStripeFee()));
        bookingData.add(new PojoBottomSheet("Down Payment", data.getDownPayment()));
        for (int i = 0; i < data.getFees().size(); i++)
            bookingData.add(new PojoBottomSheet(data.getFees().get(i).getName()
                    , Double.valueOf(data.getFees().get(i).getValue())));
        for (int i = 0; i < data.getExtraOptionsPrices().size(); i++)
            bookingData.add(new PojoBottomSheet(data.getExtraOptionsPrices().get(i).getName(),
                    Double.valueOf(data.getExtraOptionsPrices().get(i).getValue())));

        imageViewBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        CustomAdapterBottomSheet adapter = new CustomAdapterBottomSheet(getContext(), bookingData);
        listView.setAdapter(adapter);
    }

    private void connectEventApi() {
        int id = 2032;
        api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoEventList> call = api.getEvent(id);
        call.enqueue(new Callback<PojoEventList>() {
            @Override
            public void onResponse(Call<PojoEventList> call, Response<PojoEventList> response) {
                if (response.body() != null)
                    feedEventData(response.body().getData());
            }

            @Override
            public void onFailure(Call<PojoEventList> call, Throwable t) {

            }
        });

    }

    private void feedEventData(List<PojoEventList.Datum> data) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerViewEvent.setLayoutManager(linearLayoutManager);

        AdapterEvent adapter = new AdapterEvent(data, getContext());
        adapter.notifyDataSetChanged();
        recyclerViewEvent.setAdapter(adapter);

    }

    private void connectWeatherApi() {
        String check_in = "10/11/2018", check_out = "08/10/2019";
        api = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoWeatherList> call = api.getWeatherList(2032, check_in, check_out);
        call.enqueue(new Callback<PojoWeatherList>() {
            @Override
            public void onResponse(Call<PojoWeatherList> call, Response<PojoWeatherList> response) {
                if (response.body() != null)
                    feedWeatherData(response.body().getData());
            }

            @Override
            public void onFailure(Call<PojoWeatherList> call, Throwable t) {
            }
        });
    }

    private void feedWeatherData(List<PojoWeatherList.Datum> data) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerViewWeather.setLayoutManager(linearLayoutManager);

        AdapterWeather adapter = new AdapterWeather(data, getContext());
        adapter.notifyDataSetChanged();
        recyclerViewWeather.setAdapter(adapter);
    }

    private void findViews() {
        backButton = view.findViewById(R.id.backButton);
        textViewCheckIn = view.findViewById(R.id.textViewCheckIn);
        textViewCheckOut = view.findViewById(R.id.textViewCheckOut);
        textViewGuests = view.findViewById(R.id.textViewGuests);
        textViewPrice = view.findViewById(R.id.textViewPrice);
        textViewTotalNights = view.findViewById(R.id.textViewTotalNights);
        textViewCheckPrice = view.findViewById(R.id.textViewCheckPrice);
        recyclerViewWeather = view.findViewById(R.id.recyclerViewWeather);
        recyclerViewEvent = view.findViewById(R.id.recyclerViewEvent);
        textViewEvents = view.findViewById(R.id.textViewEvents);
        buttonContactHost = view.findViewById(R.id.buttonContactHost);
        buttonInstantBooking = view.findViewById(R.id.buttonInstantBooking);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        buttonContactHost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity,new FragmentContactHost(),"fragment")
                .addToBackStack(null).commit();
            }
        });
        textViewCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity, new FragmentCalender(), "fragment")
                        .commit();
            }
        });
        textViewCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity, new FragmentCalender(), "fragment")
                        .commit();
            }
        });
        if (getArguments() != null) {
            Log.d(TAG, "onCreateView: " + getArguments().getString("checkOutDate"));
            textViewCheckIn.setText("" + getArguments().getString("checkInDate"));
            textViewCheckOut.setText("" + getArguments().getString("checkOutDate"));
        }
    }

}
