package com.example.vacation_manager;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoSavedListing;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterSavedListing extends RecyclerView.Adapter<AdapterSavedListing.ListHolder> {
    List<PojoSavedListing.Datum> dataList;
    Context context;
    String TAG = "AdapterSavedListing";

    public AdapterSavedListing(List<PojoSavedListing.Datum> data, Context context) {
        this.dataList = data;
        this.context = context;
    }

    @NonNull
    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_saved_listing, parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + dataList.get(position).getName());
        if (dataList.get(position).getImage().equals("")) {
            holder.textViewNoImage.setVisibility(View.VISIBLE);
        } else {
            holder.textViewNoImage.setVisibility(View.GONE);
            Picasso.with(context).load("http://vm.appening.xyz/" + dataList.get(position).getImage()).into(holder.imageViewOwnerProperty);
        }
        holder.textViewHouseName.setText(dataList.get(position).getName());
        holder.textViewHousePrice.setText("₹ " + dataList.get(position).getPricePerNight());
        holder.textViewHouseType.setText("" + dataList.get(position).getType());

       /* if (!dataList.get(position).getAverageRating().toString().equals("null"))
        holder.textViewRatingCount.setText(dataList.get(position).getAverageRating().toString());
*/
        holder.ratingBarOwner.setRating(Float.parseFloat(String.valueOf(dataList.get(position).getRatingCount())));
        holder.favouriteButton.setImageResource(R.drawable.ic_favorite_red_24dp);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        ImageView imageViewOwnerProperty, favouriteButton;
        TextView textViewHouseType, textViewHousePrice, textViewHouseName,textViewBooking, textViewNoImage,textViewRatingCount;
        AppCompatRatingBar ratingBarOwner;

        public ListHolder(@NonNull View itemView) {
            super(itemView);
            imageViewOwnerProperty = itemView.findViewById(R.id.imageViewOwnerProperty);
            textViewHouseType = itemView.findViewById(R.id.textViewHouseType);
            textViewHousePrice = itemView.findViewById(R.id.textViewHousePrice);
            textViewHouseName = itemView.findViewById(R.id.textViewHouseName);
            textViewBooking = itemView.findViewById(R.id.textViewBooking);
            ratingBarOwner = itemView.findViewById(R.id.ratingBarOwner);
            textViewRatingCount = itemView.findViewById(R.id.textViewRatingCount);
            favouriteButton = itemView.findViewById(R.id.favouriteButton);
            textViewNoImage = itemView.findViewById(R.id.textViewNoImage);
        }
    }
}
