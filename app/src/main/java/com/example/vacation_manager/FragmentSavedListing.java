package com.example.vacation_manager;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vacation_manager.Pojo.PojoSavedListing;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSavedListing extends Fragment {
    RecyclerView recyclerViewSavedList;
    View view;
    final String TAG ="FragmentSavedListing";
    List<PojoSavedListing.Datum> list;
    private Api apiCall;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_saved_listing, container, false);
        connectApi();
        return view;

    }

    private void connectApi() {
        String authorization="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFkZmU0ZTk5MzQ2NGE3YmUzYjI1YjQ1YjcxZDFiYTg5MmQ2NGU2NmZlMWMwY2Y4NWIyNGEyN2M5MjEwMGNjOWY2NzliYmNjNDQ3ZjAzNjY4In0.eyJhdWQiOiIyIiwianRpIjoiYWRmZTRlOTkzNDY0YTdiZTNiMjViNDViNzFkMWJhODkyZDY0ZTY2ZmUxYzBjZjg1YjI0YTI3YzkyMTAwY2M5ZjY3OWJiY2M0NDdmMDM2NjgiLCJpYXQiOjE1NjQ3NDkwMjksIm5iZiI6MTU2NDc0OTAyOSwiZXhwIjoxNTY3NDI3NDg5LCJzdWIiOiI0NiIsInNjb3BlcyI6WyIqIl19.S0qUdagXpNLWYFRKZiB7AQ9Hj1zVolxAWBdeQYF4JVIm31qbvTkH-VQtrWN588siPD4rOKa_Qs1tpMM_RUEsY9LB4t5Es5MNyx7wpdMD7jdY6pe6Y_spVxvonMNwDycO2RBsZrDQhT3YED6o5PsA1FCTlIejEwvpICnwN9VOIPQkuOAmQxpdnWuR7ahTaYPUpJwYXVCipHBn7gNAKNpRqbLifLmVbg_37f13akq1p-xhAqJrGFVZyELaUoA2qBBlkRLtKXDHPzGKY0FL0zFK5MlnyWdVjXPaPU4V-jo54brI3NGbxWHp8ImJTBpyUqWVfGgQ7jLbAz_YCEkHZ5rBftbqg3fUXetFxQQGBuSzG4_l2H1f5KAhwF04WHNYNPAiEHG_ldwg4UHb6v0lrPN8g3MgL8OZTp1rk_mEDvMAh7IzKUGWP_ldZsytsMStUz7YGDXd4tAvLSA1kFdVUaf2ht2RJS3oCigCFOH5Ka74S-UuvSnBHziyMUFqD_-ZupAkKDGYI5V2su8v3IonMI5BtZCvW68ApYJFyeOPKMH3MGwf-U42Uy1OLZnr58XeraEVBfB242iE1RcQFg1HGpVk0-ceWmMuSB5CLSjk_rPx_08Iuh7ay7Wrphy2vu2v1nmRtcpaNh88k8ZzHvj9YmdEX-7bSrjQmSq0_pGke3aM_TM";

        apiCall  = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoSavedListing> call = apiCall.getList(authorization);
        call.enqueue(new Callback<PojoSavedListing>() {
            @Override
            public void onResponse(Call<PojoSavedListing> call, Response<PojoSavedListing> response) {
               // if(response.body()!=null && response.isSuccessful())
                Log.d(TAG, "onResponse: ");
                    feedData(response.body().getData());
                Log.d(TAG, "onResponse: body"+response.body());
                Log.d(TAG, "onResponse: data"+response.body().getData());
            }

            @Override
            public void onFailure(Call<PojoSavedListing> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage()+" " +t.getCause());
            }
        });
    }

    private void    feedData(List<PojoSavedListing.Datum> data) {
        Log.d("AdapterSavedListing", "feedData: "+data);
        recyclerViewSavedList = view.findViewById(R.id.recyclerViewSavedList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerViewSavedList.setLayoutManager(linearLayoutManager);

        AdapterSavedListing adapter = new AdapterSavedListing(data, getContext());
        adapter.notifyDataSetChanged();
        recyclerViewSavedList.setAdapter(adapter);
    }
}
