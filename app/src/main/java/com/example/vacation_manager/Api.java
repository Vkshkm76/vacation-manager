package com.example.vacation_manager;

import com.example.vacation_manager.Pojo.PojoBookingAvailability;
import com.example.vacation_manager.Pojo.PojoEventList;
import com.example.vacation_manager.Pojo.PojoLatestProperty;
import com.example.vacation_manager.Pojo.PojoCity;
import com.example.vacation_manager.Pojo.PojoDetails;
import com.example.vacation_manager.Pojo.PojoOwnerDetails;
import com.example.vacation_manager.Pojo.PojoProperties;
import com.example.vacation_manager.Pojo.PojoSavedListing;
import com.example.vacation_manager.Pojo.PojoSearchlocationList;
import com.example.vacation_manager.Pojo.PojoWeatherList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {
    String BASE_URL = "http://vm.appening.xyz/api/";

    @GET("latest-properties")
    Call<PojoLatestProperty> getLatestProperty();

    @GET("featured-cities")
    Call<PojoCity> getCity();

    @GET("location-list")
    Call<PojoSearchlocationList> getLocationList(@Header("origin_input") String parameter);

    @Headers({"accept: application/json"})
    @POST("search/v1/properties")
    @FormUrlEncoded
    Call<PojoProperties> savePost
                            (@Field("featured_city_id")
                                     int featured_city_id,@Header("Authorization")String authorization) ;

    @GET("property/{id}")
    Call<PojoDetails> getDetailsCall(@Path("id")int id, @Header("Authorization") String authorization);

    @GET("owner/{id}")
    Call<PojoOwnerDetails> getOwnerData(@Path("id")int id);

    @Headers({"accept: application/json"})
    @GET("favourite-list")
    Call<PojoSavedListing> getList(@Header("Authorization")String auth);

    @Headers({"accept: application/json"})
    @FormUrlEncoded
    @POST("weather-list")
    Call<PojoWeatherList> getWeatherList(@Field("property_id") int id,@Field( "check_in")
            String check_in,@Field("check_out")String check_out);

    @Headers({"accept: application/json"})
    @FormUrlEncoded
    @POST("event-list")
    Call<PojoEventList> getEvent(@Field("property_id") int id);

    @Headers({"accept: application/json"})
    @FormUrlEncoded
    @POST("check-booking-availability")
    Call<PojoBookingAvailability> getAvailability(@Field("property_id") int id,@Field( "check_in")
            String check_in,@Field("check_out")String check_out, @Field("guest")int guest);

}