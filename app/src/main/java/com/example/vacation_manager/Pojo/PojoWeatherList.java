package com.example.vacation_manager.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PojoWeatherList {
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("time")
        @Expose
        private Object time;
        @SerializedName("high_temp")
        @Expose
        private Long highTemp;
        @SerializedName("low_temp")
        @Expose
        private Long lowTemp;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("icon")
        @Expose
        private String icon;

        public Object getTime() {
            return time;
        }

        public void setTime(Object time) {
            this.time = time;
        }

        public Long getHighTemp() {
            return highTemp;
        }

        public void setHighTemp(Long highTemp) {
            this.highTemp = highTemp;
        }

        public Long getLowTemp() {
            return lowTemp;
        }

        public void setLowTemp(Long lowTemp) {
            this.lowTemp = lowTemp;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}
