package com.example.vacation_manager.Pojo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@SuppressWarnings("all")
public class PojoDetails {


	@SerializedName("details")
	@Expose
	private Details details;
	@SerializedName("owner")
	@Expose
	private Owner owner;
	@SerializedName("amenities")
	@Expose
	private List<Integer> amenities = null;
	@SerializedName("custom_prices")
	@Expose
	private List<CustomPrice> customPrices = null;
	@SerializedName("extra_prices")
	@Expose
	private List<Object> extraPrices = null;
	@SerializedName("book_dates")
	@Expose
	private List<String> bookDates = null;
	@SerializedName("amenities_master")
	@Expose
	private List<AmenitiesMaster> amenitiesMaster = null;
	@SerializedName("review")
	@Expose
	private Review review;
	@SerializedName("cancellation_policy")
	@Expose
	private CancellationPolicy cancellationPolicy;

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Integer> getAmenities() {
		return amenities;
	}

	public void setAmenities(List<Integer> amenities) {
		this.amenities = amenities;
	}

	public List<CustomPrice> getCustomPrices() {
		return customPrices;
	}

	public void setCustomPrices(List<CustomPrice> customPrices) {
		this.customPrices = customPrices;
	}

	public List<Object> getExtraPrices() {
		return extraPrices;
	}

	public void setExtraPrices(List<Object> extraPrices) {
		this.extraPrices = extraPrices;
	}

	public List<String> getBookDates() {
		return bookDates;
	}

	public void setBookDates(List<String> bookDates) {
		this.bookDates = bookDates;
	}

	public List<AmenitiesMaster> getAmenitiesMaster() {
		return amenitiesMaster;
	}

	public void setAmenitiesMaster(List<AmenitiesMaster> amenitiesMaster) {
		this.amenitiesMaster = amenitiesMaster;
	}

	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public CancellationPolicy getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(CancellationPolicy cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}


	public class Details {
		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("is_favourite")
		@Expose
		private Integer is_favourite;

		public Integer getIs_favourite() {
			return is_favourite;
		}

		public void setIs_favourite(Integer is_favourite) {
			this.is_favourite = is_favourite;
		}

		@SerializedName("category_id")
		@Expose
		private Integer categoryId;
		@SerializedName("statuses_id")
		@Expose
		private Integer statusesId;
		@SerializedName("room_type_id")
		@Expose
		private Integer roomTypeId;
		@SerializedName("user_id")
		@Expose
		private Integer userId;
		@SerializedName("verify")
		@Expose
		private Integer verify;
		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("trip_type")
		@Expose
		private String tripType;
		@SerializedName("guest_number")
		@Expose
		private Integer guestNumber;
		@SerializedName("max_guest_number")
		@Expose
		private Integer maxGuestNumber;
		@SerializedName("City")
		@Expose
		private String city;
		@SerializedName("hidden_location")
		@Expose
		private String hiddenLocation;
		@SerializedName("hidden_city")
		@Expose
		private Object hiddenCity;
		@SerializedName("city_latitude")
		@Expose
		private Object cityLatitude;
		@SerializedName("city_longitude")
		@Expose
		private Object cityLongitude;
		@SerializedName("description")
		@Expose
		private String description;
		@SerializedName("property_size")
		@Expose
		private Integer propertySize;
		@SerializedName("size_unit")
		@Expose
		private String sizeUnit;
		@SerializedName("bedrooms")
		@Expose
		private Integer bedrooms;
		@SerializedName("bathrooms")
		@Expose
		private Integer bathrooms;
		@SerializedName("toilets")
		@Expose
		private Integer toilets;
		@SerializedName("check_in_hour")
		@Expose
		private String checkInHour;
		@SerializedName("check_out_hour")
		@Expose
		private String checkOutHour;
		@SerializedName("check_in_place_address")
		@Expose
		private String checkInPlaceAddress;
		@SerializedName("check_in_place")
		@Expose
		private String checkInPlace;
		@SerializedName("late_check_in")
		@Expose
		private String lateCheckIn;
		@SerializedName("optional_services")
		@Expose
		private String optionalServices;
		@SerializedName("outdoor_facilities")
		@Expose
		private String outdoorFacilities;
		@SerializedName("no_of_night")
		@Expose
		private Integer noOfNight;
		@SerializedName("extra_bed_charge")
		@Expose
		private String extraBedCharge;
		@SerializedName("address1")
		@Expose
		private String address1;
		@SerializedName("zip")
		@Expose
		private String zip;
		@SerializedName("country")
		@Expose
		private String country;
		@SerializedName("state")
		@Expose
		private String state;
		@SerializedName("apartment")
		@Expose
		private Object apartment;
		@SerializedName("latitude")
		@Expose
		private Double latitude;
		@SerializedName("longitude")
		@Expose
		private Double longitude;
		@SerializedName("property_image")
		@Expose
		private String propertyImage;
		@SerializedName("thumbnail")
		@Expose
		private Object thumbnail;
		@SerializedName("video_from")
		@Expose
		private String videoFrom;
		@SerializedName("video_id")
		@Expose
		private String videoId;
		@SerializedName("down_payment_type")
		@Expose
		private String downPaymentType;
		@SerializedName("down_payment_value")
		@Expose
		private Integer downPaymentValue;
		@SerializedName("for_business")
		@Expose
		private Object forBusiness;
		@SerializedName("for_family")
		@Expose
		private Object forFamily;
		@SerializedName("extra_price_per_guest_per_night")
		@Expose
		private Integer extraPricePerGuestPerNight;
		@SerializedName("extra_guest")
		@Expose
		private Object extraGuest;
		@SerializedName("security_deposit")
		@Expose
		private Integer securityDeposit;
		@SerializedName("security_deposit_calculation")
		@Expose
		private String securityDepositCalculation;
		@SerializedName("city_fee_calculation")
		@Expose
		private String cityFeeCalculation;
		@SerializedName("city_fee")
		@Expose
		private Integer cityFee;
		@SerializedName("minimum_days_of_booking")
		@Expose
		private Integer minimumDaysOfBooking;
		@SerializedName("cleaning_fee_calculation")
		@Expose
		private String cleaningFeeCalculation;
		@SerializedName("cleaning_fee")
		@Expose
		private Integer cleaningFee;
		@SerializedName("weekend_price")
		@Expose
		private Integer weekendPrice;
		@SerializedName("price_more_than_30_days")
		@Expose
		private Integer priceMoreThan30Days;
		@SerializedName("price_more_than_7_days")
		@Expose
		private Integer priceMoreThan7Days;
		@SerializedName("taxes_in_percentage")
		@Expose
		private Integer taxesInPercentage;
		@SerializedName("price_per_night")
		@Expose
		private Integer pricePerNight;
		@SerializedName("is_affiliate")
		@Expose
		private Integer isAffiliate;
		@SerializedName("affiliate_commission")
		@Expose
		private Integer affiliateCommission;
		@SerializedName("from_day")
		@Expose
		private Integer fromDay;
		@SerializedName("cancel_charge")
		@Expose
		private Integer cancelCharge;
		@SerializedName("house_rules")
		@Expose
		private String houseRules;
		@SerializedName("extra_detail")
		@Expose
		private String extraDetail;
		@SerializedName("allow_instant_booking")
		@Expose
		private Integer allowInstantBooking;
		@SerializedName("is_featured")
		@Expose
		private Object isFeatured;
		@SerializedName("rating")
		@Expose
		private Integer rating;
		@SerializedName("no_of_review")
		@Expose
		private Integer noOfReview;
		@SerializedName("is_active")
		@Expose
		private Integer isActive;
		@SerializedName("deleted_at")
		@Expose
		private Object deletedAt;
		@SerializedName("category")
		@Expose
		private String category;
		@SerializedName("room_type")
		@Expose
		private String roomType;
		@SerializedName("sub_images")
		@Expose
		private List<SubImage> subImages = null;
		@SerializedName("beds")
		@Expose
		private List<Bed> beds = null;
		@SerializedName("category_data")
		@Expose
		private CategoryData categoryData;
		@SerializedName("room_types")
		@Expose
		private RoomTypes roomTypes;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getCategoryId() {
			return categoryId;
		}

		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}

		public Integer getStatusesId() {
			return statusesId;
		}

		public void setStatusesId(Integer statusesId) {
			this.statusesId = statusesId;
		}

		public Integer getRoomTypeId() {
			return roomTypeId;
		}

		public void setRoomTypeId(Integer roomTypeId) {
			this.roomTypeId = roomTypeId;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public Integer getVerify() {
			return verify;
		}

		public void setVerify(Integer verify) {
			this.verify = verify;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getTripType() {
			return tripType;
		}

		public void setTripType(String tripType) {
			this.tripType = tripType;
		}

		public Integer getGuestNumber() {
			return guestNumber;
		}

		public void setGuestNumber(Integer guestNumber) {
			this.guestNumber = guestNumber;
		}

		public Integer getMaxGuestNumber() {
			return maxGuestNumber;
		}

		public void setMaxGuestNumber(Integer maxGuestNumber) {
			this.maxGuestNumber = maxGuestNumber;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getHiddenLocation() {
			return hiddenLocation;
		}

		public void setHiddenLocation(String hiddenLocation) {
			this.hiddenLocation = hiddenLocation;
		}

		public Object getHiddenCity() {
			return hiddenCity;
		}

		public void setHiddenCity(Object hiddenCity) {
			this.hiddenCity = hiddenCity;
		}

		public Object getCityLatitude() {
			return cityLatitude;
		}

		public void setCityLatitude(Object cityLatitude) {
			this.cityLatitude = cityLatitude;
		}

		public Object getCityLongitude() {
			return cityLongitude;
		}

		public void setCityLongitude(Object cityLongitude) {
			this.cityLongitude = cityLongitude;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Integer getPropertySize() {
			return propertySize;
		}

		public void setPropertySize(Integer propertySize) {
			this.propertySize = propertySize;
		}

		public String getSizeUnit() {
			return sizeUnit;
		}

		public void setSizeUnit(String sizeUnit) {
			this.sizeUnit = sizeUnit;
		}

		public Integer getBedrooms() {
			return bedrooms;
		}

		public void setBedrooms(Integer bedrooms) {
			this.bedrooms = bedrooms;
		}

		public Integer getBathrooms() {
			return bathrooms;
		}

		public void setBathrooms(Integer bathrooms) {
			this.bathrooms = bathrooms;
		}

		public Integer getToilets() {
			return toilets;
		}

		public void setToilets(Integer toilets) {
			this.toilets = toilets;
		}

		public String getCheckInHour() {
			return checkInHour;
		}

		public void setCheckInHour(String checkInHour) {
			this.checkInHour = checkInHour;
		}

		public String getCheckOutHour() {
			return checkOutHour;
		}

		public void setCheckOutHour(String checkOutHour) {
			this.checkOutHour = checkOutHour;
		}

		public String getCheckInPlaceAddress() {
			return checkInPlaceAddress;
		}

		public void setCheckInPlaceAddress(String checkInPlaceAddress) {
			this.checkInPlaceAddress = checkInPlaceAddress;
		}

		public String getCheckInPlace() {
			return checkInPlace;
		}

		public void setCheckInPlace(String checkInPlace) {
			this.checkInPlace = checkInPlace;
		}

		public String getLateCheckIn() {
			return lateCheckIn;
		}

		public void setLateCheckIn(String lateCheckIn) {
			this.lateCheckIn = lateCheckIn;
		}

		public String getOptionalServices() {
			return optionalServices;
		}

		public void setOptionalServices(String optionalServices) {
			this.optionalServices = optionalServices;
		}

		public String getOutdoorFacilities() {
			return outdoorFacilities;
		}

		public void setOutdoorFacilities(String outdoorFacilities) {
			this.outdoorFacilities = outdoorFacilities;
		}

		public Integer getNoOfNight() {
			return noOfNight;
		}

		public void setNoOfNight(Integer noOfNight) {
			this.noOfNight = noOfNight;
		}

		public String getExtraBedCharge() {
			return extraBedCharge;
		}

		public void setExtraBedCharge(String extraBedCharge) {
			this.extraBedCharge = extraBedCharge;
		}

		public String getAddress1() {
			return address1;
		}

		public void setAddress1(String address1) {
			this.address1 = address1;
		}

		public String getZip() {
			return zip;
		}

		public void setZip(String zip) {
			this.zip = zip;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public Object getApartment() {
			return apartment;
		}

		public void setApartment(Object apartment) {
			this.apartment = apartment;
		}

		public Double getLatitude() {
			return latitude;
		}

		public void setLatitude(Double latitude) {
			this.latitude = latitude;
		}

		public Double getLongitude() {
			return longitude;
		}

		public void setLongitude(Double longitude) {
			this.longitude = longitude;
		}

		public String getPropertyImage() {
			return propertyImage;
		}

		public void setPropertyImage(String propertyImage) {
			this.propertyImage = propertyImage;
		}

		public Object getThumbnail() {
			return thumbnail;
		}

		public void setThumbnail(Object thumbnail) {
			this.thumbnail = thumbnail;
		}

		public String getVideoFrom() {
			return videoFrom;
		}

		public void setVideoFrom(String videoFrom) {
			this.videoFrom = videoFrom;
		}

		public String getVideoId() {
			return videoId;
		}

		public void setVideoId(String videoId) {
			this.videoId = videoId;
		}

		public String getDownPaymentType() {
			return downPaymentType;
		}

		public void setDownPaymentType(String downPaymentType) {
			this.downPaymentType = downPaymentType;
		}

		public Integer getDownPaymentValue() {
			return downPaymentValue;
		}

		public void setDownPaymentValue(Integer downPaymentValue) {
			this.downPaymentValue = downPaymentValue;
		}

		public Object getForBusiness() {
			return forBusiness;
		}

		public void setForBusiness(Object forBusiness) {
			this.forBusiness = forBusiness;
		}

		public Object getForFamily() {
			return forFamily;
		}

		public void setForFamily(Object forFamily) {
			this.forFamily = forFamily;
		}

		public Integer getExtraPricePerGuestPerNight() {
			return extraPricePerGuestPerNight;
		}

		public void setExtraPricePerGuestPerNight(Integer extraPricePerGuestPerNight) {
			this.extraPricePerGuestPerNight = extraPricePerGuestPerNight;
		}

		public Object getExtraGuest() {
			return extraGuest;
		}

		public void setExtraGuest(Object extraGuest) {
			this.extraGuest = extraGuest;
		}

		public Integer getSecurityDeposit() {
			return securityDeposit;
		}

		public void setSecurityDeposit(Integer securityDeposit) {
			this.securityDeposit = securityDeposit;
		}

		public String getSecurityDepositCalculation() {
			return securityDepositCalculation;
		}

		public void setSecurityDepositCalculation(String securityDepositCalculation) {
			this.securityDepositCalculation = securityDepositCalculation;
		}

		public String getCityFeeCalculation() {
			return cityFeeCalculation;
		}

		public void setCityFeeCalculation(String cityFeeCalculation) {
			this.cityFeeCalculation = cityFeeCalculation;
		}

		public Integer getCityFee() {
			return cityFee;
		}

		public void setCityFee(Integer cityFee) {
			this.cityFee = cityFee;
		}

		public Integer getMinimumDaysOfBooking() {
			return minimumDaysOfBooking;
		}

		public void setMinimumDaysOfBooking(Integer minimumDaysOfBooking) {
			this.minimumDaysOfBooking = minimumDaysOfBooking;
		}

		public String getCleaningFeeCalculation() {
			return cleaningFeeCalculation;
		}

		public void setCleaningFeeCalculation(String cleaningFeeCalculation) {
			this.cleaningFeeCalculation = cleaningFeeCalculation;
		}

		public Integer getCleaningFee() {
			return cleaningFee;
		}

		public void setCleaningFee(Integer cleaningFee) {
			this.cleaningFee = cleaningFee;
		}

		public Integer getWeekendPrice() {
			return weekendPrice;
		}

		public void setWeekendPrice(Integer weekendPrice) {
			this.weekendPrice = weekendPrice;
		}

		public Integer getPriceMoreThan30Days() {
			return priceMoreThan30Days;
		}

		public void setPriceMoreThan30Days(Integer priceMoreThan30Days) {
			this.priceMoreThan30Days = priceMoreThan30Days;
		}

		public Integer getPriceMoreThan7Days() {
			return priceMoreThan7Days;
		}

		public void setPriceMoreThan7Days(Integer priceMoreThan7Days) {
			this.priceMoreThan7Days = priceMoreThan7Days;
		}

		public Integer getTaxesInPercentage() {
			return taxesInPercentage;
		}

		public void setTaxesInPercentage(Integer taxesInPercentage) {
			this.taxesInPercentage = taxesInPercentage;
		}

		public Integer getPricePerNight() {
			return pricePerNight;
		}

		public void setPricePerNight(Integer pricePerNight) {
			this.pricePerNight = pricePerNight;
		}

		public Integer getIsAffiliate() {
			return isAffiliate;
		}

		public void setIsAffiliate(Integer isAffiliate) {
			this.isAffiliate = isAffiliate;
		}

		public Integer getAffiliateCommission() {
			return affiliateCommission;
		}

		public void setAffiliateCommission(Integer affiliateCommission) {
			this.affiliateCommission = affiliateCommission;
		}

		public Integer getFromDay() {
			return fromDay;
		}

		public void setFromDay(Integer fromDay) {
			this.fromDay = fromDay;
		}

		public Integer getCancelCharge() {
			return cancelCharge;
		}

		public void setCancelCharge(Integer cancelCharge) {
			this.cancelCharge = cancelCharge;
		}

		public String getHouseRules() {
			return houseRules;
		}

		public void setHouseRules(String houseRules) {
			this.houseRules = houseRules;
		}

		public String getExtraDetail() {
			return extraDetail;
		}

		public void setExtraDetail(String extraDetail) {
			this.extraDetail = extraDetail;
		}

		public Integer getAllowInstantBooking() {
			return allowInstantBooking;
		}

		public void setAllowInstantBooking(Integer allowInstantBooking) {
			this.allowInstantBooking = allowInstantBooking;
		}

		public Object getIsFeatured() {
			return isFeatured;
		}

		public void setIsFeatured(Object isFeatured) {
			this.isFeatured = isFeatured;
		}

		public Integer getRating() {
			return rating;
		}

		public void setRating(Integer rating) {
			this.rating = rating;
		}

		public Integer getNoOfReview() {
			return noOfReview;
		}

		public void setNoOfReview(Integer noOfReview) {
			this.noOfReview = noOfReview;
		}

		public Integer getIsActive() {
			return isActive;
		}

		public void setIsActive(Integer isActive) {
			this.isActive = isActive;
		}

		public Object getDeletedAt() {
			return deletedAt;
		}

		public void setDeletedAt(Object deletedAt) {
			this.deletedAt = deletedAt;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getRoomType() {
			return roomType;
		}

		public void setRoomType(String roomType) {
			this.roomType = roomType;
		}

		public List<SubImage> getSubImages() {
			return subImages;
		}

		public void setSubImages(List<SubImage> subImages) {
			this.subImages = subImages;
		}

		public List<Bed> getBeds() {
			return beds;
		}

		public void setBeds(List<Bed> beds) {
			this.beds = beds;
		}

		public CategoryData getCategoryData() {
			return categoryData;
		}

		public void setCategoryData(CategoryData categoryData) {
			this.categoryData = categoryData;
		}

		public RoomTypes getRoomTypes() {
			return roomTypes;
		}

		public void setRoomTypes(RoomTypes roomTypes) {
			this.roomTypes = roomTypes;
		}


	}


	public class Owner {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("role_id")
		@Expose
		private Integer roleId;
		@SerializedName("user_role")
		@Expose
		private Integer userRole;
		@SerializedName("plan_id")
		@Expose
		private Integer planId;
		@SerializedName("f_name")
		@Expose
		private String fName;
		@SerializedName("l_name")
		@Expose
		private String lName;
		@SerializedName("email")
		@Expose
		private String email;
		@SerializedName("avatar")
		@Expose
		private String avatar;
		@SerializedName("language")
		@Expose
		private Object language;
		@SerializedName("about")
		@Expose
		private Object about;
		@SerializedName("city")
		@Expose
		private Object city;
		@SerializedName("provider")
		@Expose
		private Object provider;
		@SerializedName("social_id")
		@Expose
		private Object socialId;
		@SerializedName("term")
		@Expose
		private Integer term;
		@SerializedName("settings")
		@Expose
		private Object settings;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("plan_type")
		@Expose
		private String planType;
		@SerializedName("feature")
		@Expose
		private Object feature;
		@SerializedName("non_feature")
		@Expose
		private Object nonFeature;
		@SerializedName("is_payment")
		@Expose
		private Integer isPayment;
		@SerializedName("is_plan_active")
		@Expose
		private Integer isPlanActive;
		@SerializedName("is_trial_plan")
		@Expose
		private Integer isTrialPlan;
		@SerializedName("stripe_customer_id")
		@Expose
		private String stripeCustomerId;
		@SerializedName("stripe_subscription_id")
		@Expose
		private String stripeSubscriptionId;
		@SerializedName("stripe_card_id")
		@Expose
		private String stripeCardId;
		@SerializedName("stripe_custom_account_id")
		@Expose
		private String stripeCustomAccountId;
		@SerializedName("latitude")
		@Expose
		private String latitude;
		@SerializedName("longitude")
		@Expose
		private String longitude;
		@SerializedName("gender")
		@Expose
		private Integer gender;
		@SerializedName("year")
		@Expose
		private Integer year;
		@SerializedName("day")
		@Expose
		private Integer day;
		@SerializedName("month")
		@Expose
		private Integer month;
		@SerializedName("contact_no")
		@Expose
		private Object contactNo;
		@SerializedName("place")
		@Expose
		private Object place;
		@SerializedName("fb_url")
		@Expose
		private Object fbUrl;
		@SerializedName("twitter_url")
		@Expose
		private Object twitterUrl;
		@SerializedName("instagram_url")
		@Expose
		private Object instagramUrl;
		@SerializedName("skype_url")
		@Expose
		private Object skypeUrl;
		@SerializedName("linkedin_url")
		@Expose
		private Object linkedinUrl;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getRoleId() {
			return roleId;
		}

		public void setRoleId(Integer roleId) {
			this.roleId = roleId;
		}

		public Integer getUserRole() {
			return userRole;
		}

		public void setUserRole(Integer userRole) {
			this.userRole = userRole;
		}

		public Integer getPlanId() {
			return planId;
		}

		public void setPlanId(Integer planId) {
			this.planId = planId;
		}

		public String getFName() {
			return fName;
		}

		public void setFName(String fName) {
			this.fName = fName;
		}

		public String getLName() {
			return lName;
		}

		public void setLName(String lName) {
			this.lName = lName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public Object getLanguage() {
			return language;
		}

		public void setLanguage(Object language) {
			this.language = language;
		}

		public Object getAbout() {
			return about;
		}

		public void setAbout(Object about) {
			this.about = about;
		}

		public Object getCity() {
			return city;
		}

		public void setCity(Object city) {
			this.city = city;
		}

		public Object getProvider() {
			return provider;
		}

		public void setProvider(Object provider) {
			this.provider = provider;
		}

		public Object getSocialId() {
			return socialId;
		}

		public void setSocialId(Object socialId) {
			this.socialId = socialId;
		}

		public Integer getTerm() {
			return term;
		}

		public void setTerm(Integer term) {
			this.term = term;
		}

		public Object getSettings() {
			return settings;
		}

		public void setSettings(Object settings) {
			this.settings = settings;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public String getPlanType() {
			return planType;
		}

		public void setPlanType(String planType) {
			this.planType = planType;
		}

		public Object getFeature() {
			return feature;
		}

		public void setFeature(Object feature) {
			this.feature = feature;
		}

		public Object getNonFeature() {
			return nonFeature;
		}

		public void setNonFeature(Object nonFeature) {
			this.nonFeature = nonFeature;
		}

		public Integer getIsPayment() {
			return isPayment;
		}

		public void setIsPayment(Integer isPayment) {
			this.isPayment = isPayment;
		}

		public Integer getIsPlanActive() {
			return isPlanActive;
		}

		public void setIsPlanActive(Integer isPlanActive) {
			this.isPlanActive = isPlanActive;
		}

		public Integer getIsTrialPlan() {
			return isTrialPlan;
		}

		public void setIsTrialPlan(Integer isTrialPlan) {
			this.isTrialPlan = isTrialPlan;
		}

		public String getStripeCustomerId() {
			return stripeCustomerId;
		}

		public void setStripeCustomerId(String stripeCustomerId) {
			this.stripeCustomerId = stripeCustomerId;
		}

		public String getStripeSubscriptionId() {
			return stripeSubscriptionId;
		}

		public void setStripeSubscriptionId(String stripeSubscriptionId) {
			this.stripeSubscriptionId = stripeSubscriptionId;
		}

		public String getStripeCardId() {
			return stripeCardId;
		}

		public void setStripeCardId(String stripeCardId) {
			this.stripeCardId = stripeCardId;
		}

		public String getStripeCustomAccountId() {
			return stripeCustomAccountId;
		}

		public void setStripeCustomAccountId(String stripeCustomAccountId) {
			this.stripeCustomAccountId = stripeCustomAccountId;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public Integer getGender() {
			return gender;
		}

		public void setGender(Integer gender) {
			this.gender = gender;
		}

		public Integer getYear() {
			return year;
		}

		public void setYear(Integer year) {
			this.year = year;
		}

		public Integer getDay() {
			return day;
		}

		public void setDay(Integer day) {
			this.day = day;
		}

		public Integer getMonth() {
			return month;
		}

		public void setMonth(Integer month) {
			this.month = month;
		}

		public Object getContactNo() {
			return contactNo;
		}

		public void setContactNo(Object contactNo) {
			this.contactNo = contactNo;
		}

		public Object getPlace() {
			return place;
		}

		public void setPlace(Object place) {
			this.place = place;
		}

		public Object getFbUrl() {
			return fbUrl;
		}

		public void setFbUrl(Object fbUrl) {
			this.fbUrl = fbUrl;
		}

		public Object getTwitterUrl() {
			return twitterUrl;
		}

		public void setTwitterUrl(Object twitterUrl) {
			this.twitterUrl = twitterUrl;
		}

		public Object getInstagramUrl() {
			return instagramUrl;
		}

		public void setInstagramUrl(Object instagramUrl) {
			this.instagramUrl = instagramUrl;
		}

		public Object getSkypeUrl() {
			return skypeUrl;
		}

		public void setSkypeUrl(Object skypeUrl) {
			this.skypeUrl = skypeUrl;
		}

		public Object getLinkedinUrl() {
			return linkedinUrl;
		}

		public void setLinkedinUrl(Object linkedinUrl) {
			this.linkedinUrl = linkedinUrl;
		}

	}

	public class Review {

		@SerializedName("total_count")
		@Expose
		private Integer totalCount;
		@SerializedName("average_rating")
		@Expose
		private Object averageRating;
		@SerializedName("accuracy_rating")
		@Expose
		private Object accuracyRating;
		@SerializedName("communication_rating")
		@Expose
		private Object communicationRating;
		@SerializedName("cleanliness_rating")
		@Expose
		private Object cleanlinessRating;
		@SerializedName("location_rating")
		@Expose
		private Object locationRating;
		@SerializedName("check_in_rating")
		@Expose
		private Object checkInRating;
		@SerializedName("value_rating")
		@Expose
		private Object valueRating;
		@SerializedName("data")
		@Expose
//		private List<Object> data = null;
		private List<Datum> data = null;
		public Integer getTotalCount() {
			return totalCount;
		}

		public void setTotalCount(Integer totalCount) {
			this.totalCount = totalCount;
		}

		public Object getAverageRating() {
			return averageRating;
		}

		public void setAverageRating(Object averageRating) {
			this.averageRating = averageRating;
		}

		public Object getAccuracyRating() {
			return accuracyRating;
		}

		public void setAccuracyRating(Object accuracyRating) {
			this.accuracyRating = accuracyRating;
		}

		public Object getCommunicationRating() {
			return communicationRating;
		}

		public void setCommunicationRating(Object communicationRating) {
			this.communicationRating = communicationRating;
		}

		public Object getCleanlinessRating() {
			return cleanlinessRating;
		}

		public void setCleanlinessRating(Object cleanlinessRating) {
			this.cleanlinessRating = cleanlinessRating;
		}

		public Object getLocationRating() {
			return locationRating;
		}

		public void setLocationRating(Object locationRating) {
			this.locationRating = locationRating;
		}

		public Object getCheckInRating() {
			return checkInRating;
		}

		public void setCheckInRating(Object checkInRating) {
			this.checkInRating = checkInRating;
		}

		public Object getValueRating() {
			return valueRating;
		}

		public void setValueRating(Object valueRating) {
			this.valueRating = valueRating;
		}

		public List<Datum> getData() {
			return data;
		}

		public void setData(List<Datum> data) {
			this.data = data;
		}

	}
	public class User {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("role_id")
		@Expose
		private Integer roleId;
		@SerializedName("user_role")
		@Expose
		private Integer userRole;
		@SerializedName("plan_id")
		@Expose
		private Integer planId;
		@SerializedName("f_name")
		@Expose
		private String fName;
		@SerializedName("l_name")
		@Expose
		private String lName;
		@SerializedName("email")
		@Expose
		private String email;
		@SerializedName("avatar")
		@Expose
		private String avatar;
		@SerializedName("language")
		@Expose
		private String language;
		@SerializedName("about")
		@Expose
		private String about;
		@SerializedName("city")
		@Expose
		private String city;
		@SerializedName("provider")
		@Expose
		private Object provider;
		@SerializedName("social_id")
		@Expose
		private Object socialId;
		@SerializedName("term")
		@Expose
		private Integer term;
		@SerializedName("settings")
		@Expose
		private Object settings;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("plan_type")
		@Expose
		private String planType;
		@SerializedName("feature")
		@Expose
		private Object feature;
		@SerializedName("non_feature")
		@Expose
		private Object nonFeature;
		@SerializedName("is_payment")
		@Expose
		private Integer isPayment;
		@SerializedName("is_plan_active")
		@Expose
		private Integer isPlanActive;
		@SerializedName("is_trial_plan")
		@Expose
		private Integer isTrialPlan;
		@SerializedName("stripe_customer_id")
		@Expose
		private String stripeCustomerId;
		@SerializedName("stripe_subscription_id")
		@Expose
		private String stripeSubscriptionId;
		@SerializedName("stripe_card_id")
		@Expose
		private String stripeCardId;
		@SerializedName("stripe_custom_account_id")
		@Expose
		private String stripeCustomAccountId;
		@SerializedName("latitude")
		@Expose
		private String latitude;
		@SerializedName("longitude")
		@Expose
		private String longitude;
		@SerializedName("gender")
		@Expose
		private Integer gender;
		@SerializedName("year")
		@Expose
		private Integer year;
		@SerializedName("day")
		@Expose
		private Integer day;
		@SerializedName("month")
		@Expose
		private Integer month;
		@SerializedName("contact_no")
		@Expose
		private String contactNo;
		@SerializedName("place")
		@Expose
		private Object place;
		@SerializedName("fb_url")
		@Expose
		private String fbUrl;
		@SerializedName("twitter_url")
		@Expose
		private String twitterUrl;
		@SerializedName("instagram_url")
		@Expose
		private String instagramUrl;
		@SerializedName("skype_url")
		@Expose
		private String skypeUrl;
		@SerializedName("linkedin_url")
		@Expose
		private String linkedinUrl;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getRoleId() {
			return roleId;
		}

		public void setRoleId(Integer roleId) {
			this.roleId = roleId;
		}

		public Integer getUserRole() {
			return userRole;
		}

		public void setUserRole(Integer userRole) {
			this.userRole = userRole;
		}

		public Integer getPlanId() {
			return planId;
		}

		public void setPlanId(Integer planId) {
			this.planId = planId;
		}

		public String getFName() {
			return fName;
		}

		public void setFName(String fName) {
			this.fName = fName;
		}

		public String getLName() {
			return lName;
		}

		public void setLName(String lName) {
			this.lName = lName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}

		public String getAbout() {
			return about;
		}

		public void setAbout(String about) {
			this.about = about;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public Object getProvider() {
			return provider;
		}

		public void setProvider(Object provider) {
			this.provider = provider;
		}

		public Object getSocialId() {
			return socialId;
		}

		public void setSocialId(Object socialId) {
			this.socialId = socialId;
		}

		public Integer getTerm() {
			return term;
		}

		public void setTerm(Integer term) {
			this.term = term;
		}

		public Object getSettings() {
			return settings;
		}

		public void setSettings(Object settings) {
			this.settings = settings;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public String getPlanType() {
			return planType;
		}

		public void setPlanType(String planType) {
			this.planType = planType;
		}

		public Object getFeature() {
			return feature;
		}

		public void setFeature(Object feature) {
			this.feature = feature;
		}

		public Object getNonFeature() {
			return nonFeature;
		}

		public void setNonFeature(Object nonFeature) {
			this.nonFeature = nonFeature;
		}

		public Integer getIsPayment() {
			return isPayment;
		}

		public void setIsPayment(Integer isPayment) {
			this.isPayment = isPayment;
		}

		public Integer getIsPlanActive() {
			return isPlanActive;
		}

		public void setIsPlanActive(Integer isPlanActive) {
			this.isPlanActive = isPlanActive;
		}

		public Integer getIsTrialPlan() {
			return isTrialPlan;
		}

		public void setIsTrialPlan(Integer isTrialPlan) {
			this.isTrialPlan = isTrialPlan;
		}

		public String getStripeCustomerId() {
			return stripeCustomerId;
		}

		public void setStripeCustomerId(String stripeCustomerId) {
			this.stripeCustomerId = stripeCustomerId;
		}

		public String getStripeSubscriptionId() {
			return stripeSubscriptionId;
		}

		public void setStripeSubscriptionId(String stripeSubscriptionId) {
			this.stripeSubscriptionId = stripeSubscriptionId;
		}

		public String getStripeCardId() {
			return stripeCardId;
		}

		public void setStripeCardId(String stripeCardId) {
			this.stripeCardId = stripeCardId;
		}

		public String getStripeCustomAccountId() {
			return stripeCustomAccountId;
		}

		public void setStripeCustomAccountId(String stripeCustomAccountId) {
			this.stripeCustomAccountId = stripeCustomAccountId;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public Integer getGender() {
			return gender;
		}

		public void setGender(Integer gender) {
			this.gender = gender;
		}

		public Integer getYear() {
			return year;
		}

		public void setYear(Integer year) {
			this.year = year;
		}

		public Integer getDay() {
			return day;
		}

		public void setDay(Integer day) {
			this.day = day;
		}

		public Integer getMonth() {
			return month;
		}

		public void setMonth(Integer month) {
			this.month = month;
		}

		public String getContactNo() {
			return contactNo;
		}

		public void setContactNo(String contactNo) {
			this.contactNo = contactNo;
		}

		public Object getPlace() {
			return place;
		}

		public void setPlace(Object place) {
			this.place = place;
		}

		public String getFbUrl() {
			return fbUrl;
		}

		public void setFbUrl(String fbUrl) {
			this.fbUrl = fbUrl;
		}

		public String getTwitterUrl() {
			return twitterUrl;
		}

		public void setTwitterUrl(String twitterUrl) {
			this.twitterUrl = twitterUrl;
		}

		public String getInstagramUrl() {
			return instagramUrl;
		}

		public void setInstagramUrl(String instagramUrl) {
			this.instagramUrl = instagramUrl;
		}

		public String getSkypeUrl() {
			return skypeUrl;
		}

		public void setSkypeUrl(String skypeUrl) {
			this.skypeUrl = skypeUrl;
		}

		public String getLinkedinUrl() {
			return linkedinUrl;
		}

		public void setLinkedinUrl(String linkedinUrl) {
			this.linkedinUrl = linkedinUrl;
		}
	}

	public class Datum {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("user_id")
		@Expose
		private Integer userId;
		@SerializedName("property_id")
		@Expose
		private Integer propertyId;
		@SerializedName("booking_id")
		@Expose
		private Integer bookingId;
		@SerializedName("accuracy")
		@Expose
		private Integer accuracy;
		@SerializedName("communication")
		@Expose
		private Integer communication;
		@SerializedName("cleanliness")
		@Expose
		private Integer cleanliness;
		@SerializedName("location")
		@Expose
		private Integer location;
		@SerializedName("check_in")
		@Expose
		private Integer checkIn;
		@SerializedName("value")
		@Expose
		private Integer value;
		@SerializedName("overall")
		@Expose
		private Integer overall;
		@SerializedName("review")
		@Expose
		private String review;
		@SerializedName("like")
		@Expose
		private Integer like;
		@SerializedName("dislike")
		@Expose
		private Integer dislike;
		@SerializedName("total")
		@Expose
		private Integer total;
		@SerializedName("percentage")
		@Expose
		private Integer percentage;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("user")
		@Expose
		private User user;
		@SerializedName("review_helpfull")
		@Expose
		private Object reviewHelpfull;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public Integer getPropertyId() {
			return propertyId;
		}

		public void setPropertyId(Integer propertyId) {
			this.propertyId = propertyId;
		}

		public Integer getBookingId() {
			return bookingId;
		}

		public void setBookingId(Integer bookingId) {
			this.bookingId = bookingId;
		}

		public Integer getAccuracy() {
			return accuracy;
		}

		public void setAccuracy(Integer accuracy) {
			this.accuracy = accuracy;
		}

		public Integer getCommunication() {
			return communication;
		}

		public void setCommunication(Integer communication) {
			this.communication = communication;
		}

		public Integer getCleanliness() {
			return cleanliness;
		}

		public void setCleanliness(Integer cleanliness) {
			this.cleanliness = cleanliness;
		}

		public Integer getLocation() {
			return location;
		}

		public void setLocation(Integer location) {
			this.location = location;
		}

		public Integer getCheckIn() {
			return checkIn;
		}

		public void setCheckIn(Integer checkIn) {
			this.checkIn = checkIn;
		}

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

		public Integer getOverall() {
			return overall;
		}

		public void setOverall(Integer overall) {
			this.overall = overall;
		}

		public String getReview() {
			return review;
		}

		public void setReview(String review) {
			this.review = review;
		}

		public Integer getLike() {
			return like;
		}

		public void setLike(Integer like) {
			this.like = like;
		}

		public Integer getDislike() {
			return dislike;
		}

		public void setDislike(Integer dislike) {
			this.dislike = dislike;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getPercentage() {
			return percentage;
		}

		public void setPercentage(Integer percentage) {
			this.percentage = percentage;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public Object getReviewHelpfull() {
			return reviewHelpfull;
		}

		public void setReviewHelpfull(Object reviewHelpfull) {
			this.reviewHelpfull = reviewHelpfull;
		}

	}
	public class RoomTypes {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

	}

	public class SubImage {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("sub_image_name")
		@Expose
		private String subImageName;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getSubImageName() {
			return subImageName;
		}

		public void setSubImageName(String subImageName) {
			this.subImageName = subImageName;
		}

	}

	public class CustomPrice {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("property_id")
		@Expose
		private Integer propertyId;
		@SerializedName("start_date")
		@Expose
		private String startDate;
		@SerializedName("end_date")
		@Expose
		private String endDate;
		@SerializedName("custom_price_per_night")
		@Expose
		private Integer customPricePerNight;
		@SerializedName("custom_price_more_than_7_days")
		@Expose
		private Integer customPriceMoreThan7Days;
		@SerializedName("custom_weekend_price")
		@Expose
		private Integer customWeekendPrice;
		@SerializedName("custom_minimum_days_of_booking")
		@Expose
		private Integer customMinimumDaysOfBooking;
		@SerializedName("custom_price_more_than_30_days")
		@Expose
		private Integer customPriceMoreThan30Days;
		@SerializedName("custom_extra_price_per_guest_per_night")
		@Expose
		private Integer customExtraPricePerGuestPerNight;
		@SerializedName("allow_check_in_day")
		@Expose
		private String allowCheckInDay;
		@SerializedName("allow_check_out_day")
		@Expose
		private String allowCheckOutDay;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("remember_token")
		@Expose
		private Object rememberToken;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getPropertyId() {
			return propertyId;
		}

		public void setPropertyId(Integer propertyId) {
			this.propertyId = propertyId;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public Integer getCustomPricePerNight() {
			return customPricePerNight;
		}

		public void setCustomPricePerNight(Integer customPricePerNight) {
			this.customPricePerNight = customPricePerNight;
		}

		public Integer getCustomPriceMoreThan7Days() {
			return customPriceMoreThan7Days;
		}

		public void setCustomPriceMoreThan7Days(Integer customPriceMoreThan7Days) {
			this.customPriceMoreThan7Days = customPriceMoreThan7Days;
		}

		public Integer getCustomWeekendPrice() {
			return customWeekendPrice;
		}

		public void setCustomWeekendPrice(Integer customWeekendPrice) {
			this.customWeekendPrice = customWeekendPrice;
		}

		public Integer getCustomMinimumDaysOfBooking() {
			return customMinimumDaysOfBooking;
		}

		public void setCustomMinimumDaysOfBooking(Integer customMinimumDaysOfBooking) {
			this.customMinimumDaysOfBooking = customMinimumDaysOfBooking;
		}

		public Integer getCustomPriceMoreThan30Days() {
			return customPriceMoreThan30Days;
		}

		public void setCustomPriceMoreThan30Days(Integer customPriceMoreThan30Days) {
			this.customPriceMoreThan30Days = customPriceMoreThan30Days;
		}

		public Integer getCustomExtraPricePerGuestPerNight() {
			return customExtraPricePerGuestPerNight;
		}

		public void setCustomExtraPricePerGuestPerNight(Integer customExtraPricePerGuestPerNight) {
			this.customExtraPricePerGuestPerNight = customExtraPricePerGuestPerNight;
		}

		public String getAllowCheckInDay() {
			return allowCheckInDay;
		}

		public void setAllowCheckInDay(String allowCheckInDay) {
			this.allowCheckInDay = allowCheckInDay;
		}

		public String getAllowCheckOutDay() {
			return allowCheckOutDay;
		}

		public void setAllowCheckOutDay(String allowCheckOutDay) {
			this.allowCheckOutDay = allowCheckOutDay;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Object getRememberToken() {
			return rememberToken;
		}

		public void setRememberToken(Object rememberToken) {
			this.rememberToken = rememberToken;
		}

	}

	public class CategoryData {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("created_at")
		@Expose
		private Object createdAt;
		@SerializedName("updated_at")
		@Expose
		private Object updatedAt;
		@SerializedName("image")
		@Expose
		private Object image;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Object getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Object createdAt) {
			this.createdAt = createdAt;
		}

		public Object getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Object updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Object getImage() {
			return image;
		}

		public void setImage(Object image) {
			this.image = image;
		}

	}

	public class CancellationPolicy {

		@SerializedName("from_day")
		@Expose
		private Integer fromDay;
		@SerializedName("cancel_charge")
		@Expose
		private Integer cancelCharge;
		@SerializedName("house_rules")
		@Expose
		private String houseRules;
		@SerializedName("extra_detail")
		@Expose
		private String extraDetail;
		@SerializedName("allow_instant_booking")
		@Expose
		private Integer allowInstantBooking;
		@SerializedName("statuses_id")
		@Expose
		private Integer statusesId;

		public Integer getFromDay() {
			return fromDay;
		}

		public void setFromDay(Integer fromDay) {
			this.fromDay = fromDay;
		}

		public Integer getCancelCharge() {
			return cancelCharge;
		}

		public void setCancelCharge(Integer cancelCharge) {
			this.cancelCharge = cancelCharge;
		}

		public String getHouseRules() {
			return houseRules;
		}

		public void setHouseRules(String houseRules) {
			this.houseRules = houseRules;
		}

		public String getExtraDetail() {
			return extraDetail;
		}

		public void setExtraDetail(String extraDetail) {
			this.extraDetail = extraDetail;
		}

		public Integer getAllowInstantBooking() {
			return allowInstantBooking;
		}

		public void setAllowInstantBooking(Integer allowInstantBooking) {
			this.allowInstantBooking = allowInstantBooking;
		}

		public Integer getStatusesId() {
			return statusesId;
		}

		public void setStatusesId(Integer statusesId) {
			this.statusesId = statusesId;
		}

	}

	public class Bed {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("property_id")
		@Expose
		private Integer propertyId;
		@SerializedName("active")
		@Expose
		private Integer active;
		@SerializedName("double")
		@Expose
		private Integer _double;
		@SerializedName("queen")
		@Expose
		private Integer queen;
		@SerializedName("single")
		@Expose
		private Integer single;
		@SerializedName("sofa_bed")
		@Expose
		private Integer sofaBed;
		@SerializedName("king")
		@Expose
		private Integer king;
		@SerializedName("couch")
		@Expose
		private Integer couch;
		@SerializedName("bunk_bed")
		@Expose
		private Integer bunkBed;
		@SerializedName("floor_mattress")
		@Expose
		private Integer floorMattress;
		@SerializedName("air_mattress")
		@Expose
		private Integer airMattress;
		@SerializedName("crib")
		@Expose
		private Integer crib;
		@SerializedName("toddler_bed")
		@Expose
		private Integer toddlerBed;
		@SerializedName("hammock")
		@Expose
		private Integer hammock;
		@SerializedName("water_bed")
		@Expose
		private Integer waterBed;
		@SerializedName("total_beds")
		@Expose
		private Integer totalBeds;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("remember_token")
		@Expose
		private Object rememberToken;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getPropertyId() {
			return propertyId;
		}

		public void setPropertyId(Integer propertyId) {
			this.propertyId = propertyId;
		}

		public Integer getActive() {
			return active;
		}

		public void setActive(Integer active) {
			this.active = active;
		}

		public Integer getDouble() {
			return _double;
		}

		public void setDouble(Integer _double) {
			this._double = _double;
		}

		public Integer getQueen() {
			return queen;
		}

		public void setQueen(Integer queen) {
			this.queen = queen;
		}

		public Integer getSingle() {
			return single;
		}

		public void setSingle(Integer single) {
			this.single = single;
		}

		public Integer getSofaBed() {
			return sofaBed;
		}

		public void setSofaBed(Integer sofaBed) {
			this.sofaBed = sofaBed;
		}

		public Integer getKing() {
			return king;
		}

		public void setKing(Integer king) {
			this.king = king;
		}

		public Integer getCouch() {
			return couch;
		}

		public void setCouch(Integer couch) {
			this.couch = couch;
		}

		public Integer getBunkBed() {
			return bunkBed;
		}

		public void setBunkBed(Integer bunkBed) {
			this.bunkBed = bunkBed;
		}

		public Integer getFloorMattress() {
			return floorMattress;
		}

		public void setFloorMattress(Integer floorMattress) {
			this.floorMattress = floorMattress;
		}

		public Integer getAirMattress() {
			return airMattress;
		}

		public void setAirMattress(Integer airMattress) {
			this.airMattress = airMattress;
		}

		public Integer getCrib() {
			return crib;
		}

		public void setCrib(Integer crib) {
			this.crib = crib;
		}

		public Integer getToddlerBed() {
			return toddlerBed;
		}

		public void setToddlerBed(Integer toddlerBed) {
			this.toddlerBed = toddlerBed;
		}

		public Integer getHammock() {
			return hammock;
		}

		public void setHammock(Integer hammock) {
			this.hammock = hammock;
		}

		public Integer getWaterBed() {
			return waterBed;
		}

		public void setWaterBed(Integer waterBed) {
			this.waterBed = waterBed;
		}

		public Integer getTotalBeds() {
			return totalBeds;
		}

		public void setTotalBeds(Integer totalBeds) {
			this.totalBeds = totalBeds;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Object getRememberToken() {
			return rememberToken;
		}

		public void setRememberToken(Object rememberToken) {
			this.rememberToken = rememberToken;
		}

	}


	public static class AmenitiesMaster {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("type")
		@Expose
		private String type;
		@SerializedName("icon")
		@Expose
		private String icon;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

	}
}