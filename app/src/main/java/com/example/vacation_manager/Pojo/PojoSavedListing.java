package com.example.vacation_manager.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
@SuppressWarnings("all")
public class PojoSavedListing {
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("property_id")
        @Expose
        private Integer propertyId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("price_per_night")
        @Expose
        private Integer pricePerNight;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("instant_booking")
        @Expose
        private Boolean instantBooking;
        @SerializedName("no_of_beds")
        @Expose
        private Integer noOfBeds;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("rating_count")
        @Expose
        private Integer ratingCount;
        @SerializedName("average_rating")
        @Expose
        private Object averageRating;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(Integer propertyId) {
            this.propertyId = propertyId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getPricePerNight() {
            return pricePerNight;
        }

        public void setPricePerNight(Integer pricePerNight) {
            this.pricePerNight = pricePerNight;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public Boolean getInstantBooking() {
            return instantBooking;
        }

        public void setInstantBooking(Boolean instantBooking) {
            this.instantBooking = instantBooking;
        }

        public Integer getNoOfBeds() {
            return noOfBeds;
        }

        public void setNoOfBeds(Integer noOfBeds) {
            this.noOfBeds = noOfBeds;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Integer getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(Integer ratingCount) {
            this.ratingCount = ratingCount;
        }

        public Object getAverageRating() {
            return averageRating;
        }

        public void setAverageRating(Object averageRating) {
            this.averageRating = averageRating;
        }

    }
}
