package com.example.vacation_manager.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
@SuppressWarnings("all")
public class PojoOwnerDetails {

    @SerializedName("detail")
    @Expose
    private Detail detail;
    @SerializedName("property_list")
    @Expose
    private List<PropertyList> propertyList = null;
    @SerializedName("reviews")
    @Expose
    private Reviews reviews;

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }


    public List<PropertyList> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<PropertyList> propertyList) {
        this.propertyList = propertyList;
    }

    public Reviews getReviews() {
        return reviews;
    }

    public void setReviews(Reviews reviews) {
        this.reviews = reviews;
    }

    public class PropertyList {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("price_per_night")
        @Expose
        private Integer pricePerNight;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("instant_booking")
        @Expose
        private Boolean instantBooking;
        @SerializedName("no_of_beds")
        @Expose
        private Integer noOfBeds;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("rating_count")
        @Expose
        private Integer ratingCount;
        @SerializedName("average_rating")
        @Expose
        private Object averageRating;
        @SerializedName("sub_image")
        @Expose
        private List<SubImage> subImage = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getPricePerNight() {
            return pricePerNight;
        }

        public void setPricePerNight(Integer pricePerNight) {
            this.pricePerNight = pricePerNight;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public Boolean getInstantBooking() {
            return instantBooking;
        }

        public void setInstantBooking(Boolean instantBooking) {
            this.instantBooking = instantBooking;
        }

        public Integer getNoOfBeds() {
            return noOfBeds;
        }

        public void setNoOfBeds(Integer noOfBeds) {
            this.noOfBeds = noOfBeds;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Integer getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(Integer ratingCount) {
            this.ratingCount = ratingCount;
        }

        public Object getAverageRating() {
            return averageRating;
        }

        public void setAverageRating(Object averageRating) {
            this.averageRating = averageRating;
        }

        public List<SubImage> getSubImage() {
            return subImage;
        }

        public void setSubImage(List<SubImage> subImage) {
            this.subImage = subImage;
        }

    }
    public class SubImage {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("sub_image_name")
        @Expose
        private String subImageName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getSubImageName() {
            return subImageName;
        }

        public void setSubImageName(String subImageName) {
            this.subImageName = subImageName;
        }

    }
    public class Detail {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("f_name")
        @Expose
        private String fName;
        @SerializedName("l_name")
        @Expose
        private String lName;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("language")
        @Expose
        private Object language;
        @SerializedName("about")
        @Expose
        private Object about;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFName() {
            return fName;
        }

        public void setFName(String fName) {
            this.fName = fName;
        }

        public String getLName() {
            return lName;
        }

        public void setLName(String lName) {
            this.lName = lName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public Object getLanguage() {
            return language;
        }

        public void setLanguage(Object language) {
            this.language = language;
        }

        public Object getAbout() {
            return about;
        }

        public void setAbout(Object about) {
            this.about = about;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("owner_id")
        @Expose
        private Integer ownerId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("review")
        @Expose
        private String review;
        @SerializedName("is_active")
        @Expose
        private Integer isActive;
        @SerializedName("created_at")
        @Expose
        private Object createdAt;
        @SerializedName("updated_at")
        @Expose
        private Object updatedAt;
        @SerializedName("user")
        @Expose
        private User user;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(Integer ownerId) {
            this.ownerId = ownerId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public Object getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Object createdAt) {
            this.createdAt = createdAt;
        }

        public Object getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

    }
    public class Reviews {

        @SerializedName("total_count")
        @Expose
        private Integer totalCount;
        @SerializedName("average_rating")
        @Expose
        private Double averageRating;
        @SerializedName("data")
        @Expose
        private List<Datum> data = null;

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public Double getAverageRating() {
            return averageRating;
        }

        public void setAverageRating(Double averageRating) {
            this.averageRating = averageRating;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("role_id")
        @Expose
        private Integer roleId;
        @SerializedName("user_role")
        @Expose
        private Integer userRole;
        @SerializedName("plan_id")
        @Expose
        private Object planId;
        @SerializedName("f_name")
        @Expose
        private String fName;
        @SerializedName("l_name")
        @Expose
        private String lName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("language")
        @Expose
        private Object language;
        @SerializedName("about")
        @Expose
        private Object about;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("provider")
        @Expose
        private Object provider;
        @SerializedName("social_id")
        @Expose
        private Object socialId;
        @SerializedName("term")
        @Expose
        private Integer term;
        @SerializedName("settings")
        @Expose
        private Object settings;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("plan_type")
        @Expose
        private Object planType;
        @SerializedName("feature")
        @Expose
        private Object feature;
        @SerializedName("non_feature")
        @Expose
        private Object nonFeature;
        @SerializedName("is_payment")
        @Expose
        private Integer isPayment;
        @SerializedName("is_plan_active")
        @Expose
        private Integer isPlanActive;
        @SerializedName("is_trial_plan")
        @Expose
        private Integer isTrialPlan;
        @SerializedName("stripe_customer_id")
        @Expose
        private String stripeCustomerId;
        @SerializedName("stripe_subscription_id")
        @Expose
        private Object stripeSubscriptionId;
        @SerializedName("stripe_card_id")
        @Expose
        private Object stripeCardId;
        @SerializedName("stripe_custom_account_id")
        @Expose
        private Object stripeCustomAccountId;
        @SerializedName("latitude")
        @Expose
        private Object latitude;
        @SerializedName("longitude")
        @Expose
        private Object longitude;
        @SerializedName("gender")
        @Expose
        private Object gender;
        @SerializedName("year")
        @Expose
        private Object year;
        @SerializedName("day")
        @Expose
        private Object day;
        @SerializedName("month")
        @Expose
        private Object month;
        @SerializedName("contact_no")
        @Expose
        private Object contactNo;
        @SerializedName("place")
        @Expose
        private Object place;
        @SerializedName("fb_url")
        @Expose
        private Object fbUrl;
        @SerializedName("twitter_url")
        @Expose
        private Object twitterUrl;
        @SerializedName("instagram_url")
        @Expose
        private Object instagramUrl;
        @SerializedName("skype_url")
        @Expose
        private Object skypeUrl;
        @SerializedName("linkedin_url")
        @Expose
        private Object linkedinUrl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRoleId() {
            return roleId;
        }

        public void setRoleId(Integer roleId) {
            this.roleId = roleId;
        }

        public Integer getUserRole() {
            return userRole;
        }

        public void setUserRole(Integer userRole) {
            this.userRole = userRole;
        }

        public Object getPlanId() {
            return planId;
        }

        public void setPlanId(Object planId) {
            this.planId = planId;
        }

        public String getFName() {
            return fName;
        }

        public void setFName(String fName) {
            this.fName = fName;
        }

        public String getLName() {
            return lName;
        }

        public void setLName(String lName) {
            this.lName = lName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public Object getLanguage() {
            return language;
        }

        public void setLanguage(Object language) {
            this.language = language;
        }

        public Object getAbout() {
            return about;
        }

        public void setAbout(Object about) {
            this.about = about;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public Object getProvider() {
            return provider;
        }

        public void setProvider(Object provider) {
            this.provider = provider;
        }

        public Object getSocialId() {
            return socialId;
        }

        public void setSocialId(Object socialId) {
            this.socialId = socialId;
        }

        public Integer getTerm() {
            return term;
        }

        public void setTerm(Integer term) {
            this.term = term;
        }

        public Object getSettings() {
            return settings;
        }

        public void setSettings(Object settings) {
            this.settings = settings;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getPlanType() {
            return planType;
        }

        public void setPlanType(Object planType) {
            this.planType = planType;
        }

        public Object getFeature() {
            return feature;
        }

        public void setFeature(Object feature) {
            this.feature = feature;
        }

        public Object getNonFeature() {
            return nonFeature;
        }

        public void setNonFeature(Object nonFeature) {
            this.nonFeature = nonFeature;
        }

        public Integer getIsPayment() {
            return isPayment;
        }

        public void setIsPayment(Integer isPayment) {
            this.isPayment = isPayment;
        }

        public Integer getIsPlanActive() {
            return isPlanActive;
        }

        public void setIsPlanActive(Integer isPlanActive) {
            this.isPlanActive = isPlanActive;
        }

        public Integer getIsTrialPlan() {
            return isTrialPlan;
        }

        public void setIsTrialPlan(Integer isTrialPlan) {
            this.isTrialPlan = isTrialPlan;
        }

        public String getStripeCustomerId() {
            return stripeCustomerId;
        }

        public void setStripeCustomerId(String stripeCustomerId) {
            this.stripeCustomerId = stripeCustomerId;
        }

        public Object getStripeSubscriptionId() {
            return stripeSubscriptionId;
        }

        public void setStripeSubscriptionId(Object stripeSubscriptionId) {
            this.stripeSubscriptionId = stripeSubscriptionId;
        }

        public Object getStripeCardId() {
            return stripeCardId;
        }

        public void setStripeCardId(Object stripeCardId) {
            this.stripeCardId = stripeCardId;
        }

        public Object getStripeCustomAccountId() {
            return stripeCustomAccountId;
        }

        public void setStripeCustomAccountId(Object stripeCustomAccountId) {
            this.stripeCustomAccountId = stripeCustomAccountId;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public Object getYear() {
            return year;
        }

        public void setYear(Object year) {
            this.year = year;
        }

        public Object getDay() {
            return day;
        }

        public void setDay(Object day) {
            this.day = day;
        }

        public Object getMonth() {
            return month;
        }

        public void setMonth(Object month) {
            this.month = month;
        }

        public Object getContactNo() {
            return contactNo;
        }

        public void setContactNo(Object contactNo) {
            this.contactNo = contactNo;
        }

        public Object getPlace() {
            return place;
        }

        public void setPlace(Object place) {
            this.place = place;
        }

        public Object getFbUrl() {
            return fbUrl;
        }

        public void setFbUrl(Object fbUrl) {
            this.fbUrl = fbUrl;
        }

        public Object getTwitterUrl() {
            return twitterUrl;
        }

        public void setTwitterUrl(Object twitterUrl) {
            this.twitterUrl = twitterUrl;
        }

        public Object getInstagramUrl() {
            return instagramUrl;
        }

        public void setInstagramUrl(Object instagramUrl) {
            this.instagramUrl = instagramUrl;
        }

        public Object getSkypeUrl() {
            return skypeUrl;
        }

        public void setSkypeUrl(Object skypeUrl) {
            this.skypeUrl = skypeUrl;
        }

        public Object getLinkedinUrl() {
            return linkedinUrl;
        }

        public void setLinkedinUrl(Object linkedinUrl) {
            this.linkedinUrl = linkedinUrl;
        }

    }
}
