package com.example.vacation_manager.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PojoBookingAvailability {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("fees")
        @Expose
        private List<Fee> fees = null;
        @SerializedName("per_night_charge")
        @Expose
        private Long perNightCharge;
        @SerializedName("sub_total")
        @Expose
        private Double subTotal;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("extra_options_prices")
        @Expose
        private List<ExtraOptionsPrice> extraOptionsPrices = null;
        @SerializedName("stripe_fee")
        @Expose
        private Double stripeFee;
        @SerializedName("down_payment")
        @Expose
        private Double downPayment;
        @SerializedName("payable_amount")
        @Expose
        private Double payableAmount;
        @SerializedName("saving")
        @Expose
        private Long saving;

        public List<Fee> getFees() {
            return fees;
        }

        public void setFees(List<Fee> fees) {
            this.fees = fees;
        }

        public Long getPerNightCharge() {
            return perNightCharge;
        }

        public void setPerNightCharge(Long perNightCharge) {
            this.perNightCharge = perNightCharge;
        }

        public Double getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Double subTotal) {
            this.subTotal = subTotal;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public List<ExtraOptionsPrice> getExtraOptionsPrices() {
            return extraOptionsPrices;
        }

        public void setExtraOptionsPrices(List<ExtraOptionsPrice> extraOptionsPrices) {
            this.extraOptionsPrices = extraOptionsPrices;
        }

        public Double getStripeFee() {
            return stripeFee;
        }

        public void setStripeFee(Double stripeFee) {
            this.stripeFee = stripeFee;
        }

        public Double getDownPayment() {
            return downPayment;
        }

        public void setDownPayment(Double downPayment) {
            this.downPayment = downPayment;
        }

        public Double getPayableAmount() {
            return payableAmount;
        }

        public void setPayableAmount(Double payableAmount) {
            this.payableAmount = payableAmount;
        }

        public Long getSaving() {
            return saving;
        }

        public void setSaving(Long saving) {
            this.saving = saving;
        }

    }

    public class ExtraOptionsPrice {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("value")
        @Expose
        private Long value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getValue() {
            return value;
        }

        public void setValue(Long value) {
            this.value = value;
        }

    }

    public class Fee {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("value")
        @Expose
        private Long value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getValue() {
            return value;
        }

        public void setValue(Long value) {
            this.value = value;
        }

    }
}
