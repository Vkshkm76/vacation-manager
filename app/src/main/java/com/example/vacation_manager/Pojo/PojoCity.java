package com.example.vacation_manager.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
@SuppressWarnings("All")
public class PojoCity {

    @SerializedName("feature_city")
    @Expose
    private List<FeatureCity> featureCity = null;

    public List<FeatureCity> getFeatureCity() {
        return featureCity;
    }

    public void setFeatureCity(List<FeatureCity> featureCity) {
        this.featureCity = featureCity;
    }


    public class FeatureCity {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("active")
        @Expose
        private Integer active;
        @SerializedName("city_image")
        @Expose
        private String cityImage;
        @SerializedName("sub_title")
        @Expose
        private String subTitle;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("radius")
        @Expose
        private Integer radius;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public String getCityImage() {
            return cityImage;
        }

        public void setCityImage(String cityImage) {
            this.cityImage = cityImage;
        }

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Integer getRadius() {
            return radius;
        }

        public void setRadius(Integer radius) {
            this.radius = radius;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}


