package com.example.vacation_manager.Map;

public class MapData{


    int priceMap;
    String nameMap, cityMap,imageMap;

    public int getPriceMap() {
        return priceMap;
    }

    public void setPriceMap(int priceMap) {
        this.priceMap = priceMap;
    }

    public String getImageMap() {
        return imageMap;
    }

    public void setImageMap(String imageMap) {
        this.imageMap = imageMap;
    }

    public String getNameMap() {
        return nameMap;
    }

    public void setNameMap(String nameMap) {
        this.nameMap = nameMap;
    }

    public String getCityMap() {
        return cityMap;
    }

    public void setCityMap(String cityMap) {
        this.cityMap = cityMap;
    }

}
