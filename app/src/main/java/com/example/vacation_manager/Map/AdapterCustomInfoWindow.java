package com.example.vacation_manager.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.example.vacation_manager.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("all")
public class AdapterCustomInfoWindow implements GoogleMap.InfoWindowAdapter, GoogleMap.OnMarkerClickListener {
    View view;
    final String TAG = "AdapterCustomInfoWindow";
    private Context context;
    MapData mapData;
    private final Map<Marker, Bitmap> images = new HashMap<>();
    private final Map<Marker, Target<Bitmap>> targets = new HashMap<>();

    public AdapterCustomInfoWindow(Context context) {
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.row_custom_info_window, null);
    }

    private void randomWindowText(final Marker marker, View view) {
        Gson gson = new Gson();
        mapData = gson.fromJson(marker.getSnippet(), MapData.class);

        TextView textViewname, textViewCity, textViewPrice;
        ImageView imageView;
        Bitmap bitmap = images.get(marker);

        textViewname = view.findViewById(R.id.nameCustom);
        imageView = view.findViewById(R.id.imageCustom);
        textViewCity = view.findViewById(R.id.cityCustom);
        textViewPrice = view.findViewById(R.id.priceCustom);

        textViewname.setText(mapData.getNameMap());
        textViewCity.setText(mapData.getCityMap());
        textViewPrice.setText("$ " + mapData.getPriceMap());

        if (bitmap == null) {
            Glide.with(context).asBitmap()
                    .load("http://vm.appening.xyz/" + mapData.getImageMap())
                    .dontAnimate()
                    .into(getTarget(marker));
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        randomWindowText(marker, view);
        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        randomWindowText(marker, view);
        return null;
    }

    private Target<Bitmap> getTarget(Marker marker) {
        Target<Bitmap> target = targets.get(marker);
        if (target == null) {
            target = new InfoTarget(marker);
            targets.put(marker, target);
        }
        return target;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return true;
    }

    private class InfoTarget extends CustomTarget<Bitmap> {
        Marker marker;

        InfoTarget(Marker marker) {
            this.marker = marker;
        }

        @Override
        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
            images.put(marker, resource);
            marker.showInfoWindow();
        }

        @Override
        public void onLoadCleared(@Nullable Drawable placeholder) {
            images.remove(marker);
        }
    }
}
