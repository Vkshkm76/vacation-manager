package com.example.vacation_manager.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vacation_manager.Pojo.PojoProperties;
import com.example.vacation_manager.R;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class AdapterMap extends RecyclerView.Adapter<AdapterMap.HolderMap> {
    private Context context;
    private List<PojoProperties.Featured> pojoFeaturedList;
    OnClickListenerMap clickListenerMap;
    public AdapterMap(List<PojoProperties.Featured> featured, Context context, OnClickListenerMap mapListener) {
        this.context=context;
        this.pojoFeaturedList = featured;
        this.clickListenerMap = mapListener;
    }
    @NonNull
    @Override
    public HolderMap onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_map_recycler,parent,false);
        return new HolderMap(view, clickListenerMap);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderMap holder, int position) {
        PojoProperties.Featured pojo= pojoFeaturedList.get(position);
        holder.textViewName.setText(pojo.getName());
        holder.city.setText(pojo.getCity());
        Transformation tr=new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(2)
                .cornerRadiusDp(5)
                .oval(false)
                .build();
        Picasso.with(context).load("http://vm.appening.xyz/".concat(pojo.getImage())).resize(488,484).into(holder.image);
        holder.textViewAverageRating.setText("("+pojo.getRatingCount().toString()+")");
        if(pojo.getAverageRating()==null){
            holder.ratingBar.setRating(0);
        }else{
            holder.ratingBar.setRating(Float.parseFloat(String.valueOf(pojo.getAverageRating())));
        }

    }
    private void randomWindowText(final Marker marker, View view){
        Gson gson= new Gson();
        MapData mapData=gson.fromJson(marker.getSnippet(), MapData.class);

        TextView textViewname,textViewCity,textViewPrice;
        String image;
        ImageView imageView;
        image = mapData.getImageMap();
        textViewname = view.findViewById(R.id.nameCustom);
        imageView = view.findViewById(R.id.imageCustom);
        textViewCity= view.findViewById(R.id.cityCustom);
        textViewPrice=view.findViewById(R.id.priceCustom);

        textViewname.setText(mapData.getNameMap());
        textViewCity.setText(mapData.getCityMap());
        textViewPrice.setText("$ "+mapData.getPriceMap());
        Glide.with(context)
                .load("http://vm.appening.xyz/" + image)
                .into(imageView);
        }

    @Override
    public int getItemCount() {
        return pojoFeaturedList.size();
    }

    public class HolderMap extends RecyclerView.ViewHolder {
        RelativeLayout row_Map;
        ImageView image;
        TextView textViewName, city,pricePerNight, textViewAverageRating;
        RatingBar ratingBar;
        OnClickListenerMap clickListenerMap;
        public HolderMap(@NonNull View itemView, final OnClickListenerMap clickListenerMap) {
            super(itemView);
            row_Map = itemView.findViewById(R.id.layoutRowMap);
            textViewName = itemView.findViewById(R.id.textViewName);
            image = itemView.findViewById(R.id.imgView);
            city = itemView.findViewById(R.id.textViewCity);
            pricePerNight =itemView.findViewById(R.id.textViewPrice);
            textViewAverageRating=itemView.findViewById(R.id.averageRating);
            ratingBar=itemView.findViewById(R.id.ratingBar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    PojoProperties.Featured id= pojoFeaturedList.get(position);
                    clickListenerMap.onClickListenerMap(id.getId());
                }
            });
        }
    }
    public interface OnClickListenerMap {
        void onClickListenerMap(int id);
    }
}
