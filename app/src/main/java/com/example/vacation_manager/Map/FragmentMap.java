package com.example.vacation_manager.Map;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vacation_manager.Pojo.PojoProperties;
import com.example.vacation_manager.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.ui.IconGenerator;

import java.util.List;

@SuppressWarnings("all")
public class FragmentMap extends Fragment implements OnMapReadyCallback, AdapterMap.OnClickListenerMap {
    private static final String PROPERTY_JSON_KEY = "PROPERTY_JSON";
    private GoogleMap mMap;
    RecyclerView recyclerMap;
    View view;
    PojoProperties pojoProperties;
    private List<PojoProperties.Featured> featuredList;
    MapData mapData;
    private static Gson gson;
    CommunicationMap communicationMap;
    private static Gson getGsonParser() {
        if (null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_map, container, false);

        String properties = getArguments() != null ? getArguments().getString(PROPERTY_JSON_KEY) : null;

        pojoProperties = getGsonParser().fromJson(properties, PojoProperties.class);
        if (pojoProperties != null) {
            featuredData(pojoProperties.getFeatured(), pojoProperties.getNonFeatured());
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        return view;
    }

    private void featuredData(List<PojoProperties.Featured> featured, List<PojoProperties.Featured> nonFeatureds) {
        recyclerMap = view.findViewById(R.id.recyclerViewMap);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.HORIZONTAL);
        recyclerMap.setLayoutManager(llm);
        featured.addAll(nonFeatureds);
        this.featuredList = featured;

        AdapterMap adapter = new AdapterMap(featured, getContext(), this);
        adapter.notifyDataSetChanged();
        recyclerMap.setAdapter(adapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int pricePerNight;
        String name, city, image;
        Double lat, lon;
        Double latitude = pojoProperties.getLatLong().get(0).getLatitude();
        Double longitude = pojoProperties.getLatLong().get(0).getLongitude();

        CameraPosition googlePlex = CameraPosition.builder().target(new LatLng(latitude, longitude))
                                    .zoom(10).bearing(0).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex));
        for (int i = 0; i < featuredList.size(); i++) {
            lat = featuredList.get(i).getLatitude();
            lon = featuredList.get(i).getLongitude();
            name = featuredList.get(i).getName();
            image = featuredList.get(i).getImage();
            city = pojoProperties.getFeatured().get(i).getCity();
            pricePerNight = featuredList.get(i).getPricePerNight();
            mMap.setInfoWindowAdapter(new AdapterCustomInfoWindow(getContext()));

            addIcon(pricePerNight, new LatLng(lat, lon), name, image, city);
        }
    }

    public void addIcon(int price, final LatLng position, String name, String image, String city) {
        mapData = new MapData();
        mapData.setNameMap(name);
        mapData.setImageMap(image);
        mapData.setPriceMap(price);
        mapData.setCityMap(city);

        Gson gson = new Gson();
        String mapSnippetData = gson.toJson(mapData);
        IconGenerator iconFactory = new IconGenerator(getContext());
        iconFactory.setTextAppearance(R.style.markerTextStyle);

        MarkerOptions markerOptions = new MarkerOptions().title(image).snippet(mapSnippetData)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconFactory.makeIcon("$ " + price))).
                        position(position).
                        anchor(0.5f, -1.5f);

        mMap.addMarker(markerOptions);
        mMap.setInfoWindowAdapter(new AdapterCustomInfoWindow(getContext()));

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicationMap = (CommunicationMap) context;
    }

    @Override
    public void onClickListenerMap(int id) {
        communicationMap.initFragmentDetail(id);
    }

    public interface CommunicationMap {
        void initFragmentDetail(int id);
    }
}