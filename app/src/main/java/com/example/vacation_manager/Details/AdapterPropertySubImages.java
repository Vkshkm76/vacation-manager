package com.example.vacation_manager.Details;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoDetails;
import com.example.vacation_manager.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class AdapterPropertySubImages extends RecyclerView.Adapter<AdapterPropertySubImages.HolderSubImage> {
    private Context context;
    public List<PojoDetails.SubImage> detail;
    public AdapterPropertySubImages(List<PojoDetails.SubImage> detail, Context context) {
        this.detail=detail;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderSubImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery_detailpage,parent,false);
        return new HolderSubImage(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderSubImage holder, int position) {
       PojoDetails.SubImage pos=detail.get(position);
        Transformation tr = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(5)
                .oval(false)
                .build();
       if(pos.getSubImageName().isEmpty()){
            holder.textViewNoImage.setVisibility(View.VISIBLE);
       }else {
           holder.textViewNoImage.setVisibility(View.GONE);
           Picasso.with(context).load("http://vm.appening.xyz/" + pos.getSubImageName())
                   .fit().transform(tr).into(holder.image);
       }
    }

    @Override
    public int getItemCount() {
        return detail.size();
    }

    public class HolderSubImage extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textViewNoImage;
        public HolderSubImage(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageViewGallery);
            textViewNoImage=itemView.findViewById(R.id.textViewNoImageSubImage);
        }
    }
}
