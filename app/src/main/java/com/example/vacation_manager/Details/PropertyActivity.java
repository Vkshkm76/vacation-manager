package com.example.vacation_manager.Details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.example.vacation_manager.Booking.FragmentAvailability;
import com.example.vacation_manager.Booking.FragmentCalender;
import com.example.vacation_manager.ContactHost.FragmentContactHost;
import com.example.vacation_manager.R;

public class PropertyActivity extends AppCompatActivity implements FragmentDetailPage.FragmentDetailPageListener
        , FragmentCalender.CalendarListener {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    final String TAG = "PropertyActivity";
    int id;
    Bundle bundle;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);
        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        loadFragment(new FragmentDetailPage(), id);

    }

    private void loadFragment(FragmentDetailPage fragmentDetailPage, int id) {
        bundle = new Bundle();
        bundle.putInt("propertyId", id);
        fragmentDetailPage = new FragmentDetailPage();
        fragmentDetailPage.setArguments(bundle);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fragmentDetailPage).addToBackStack(null);
        fragmentTransaction.commit();
    }
/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
*/
    @Override
    public void addLatLng(Double latitude, Double longitude, String title) {
        bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putDouble("latitude", latitude);
        bundle.putDouble("longitude", longitude);

        FragmentFullMap fr = new FragmentFullMap();
        fr.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fr).addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void cancellationPolicy(String title, String policy) {
        bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("policy", policy);
        FragmentCancellationPolicy fragment = new FragmentCancellationPolicy();
        fragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void ownerHostClick(int id) {
        bundle = new Bundle();
        bundle.putInt("id", id);
        FragmentOwner fragment = new FragmentOwner();
        fragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void loadBookingAvailability() {
        String back=FragmentDetailPage.class.getClass().getName();
                FragmentAvailability fragmentAvailability = new FragmentAvailability();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fragmentAvailability).addToBackStack(back).commit();

    }

    @Override
    public void onDateRangeSelected(String startDate, String endDate) {
        bundle = new Bundle();
        bundle.putString("checkInDate", startDate);
        bundle.putString("checkOutDate", endDate);

        FragmentAvailability fragmentAvailability = new FragmentAvailability();
        fragmentAvailability.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameSecondActivity, fragmentAvailability).commit();
       /* if (getSupportFragmentManager().findFragmentByTag(FragmentAvailability.class.getSimpleName()) == null) {
            FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            t.attach(getSupportFragmentManager().findFragmentByTag(FragmentAvailability.class.getSimpleName()));
            t.commit();

        } else {
            FragmentTransaction t = getSupportFragmentManager().beginTransaction();
//            t.attach(getSupportFragmentManager().findFragmentByTag(FragmentAvailability.class.getSimpleName()));
             fragmentTransaction.detach(getSupportFragmentManager().findFragmentByTag(FragmentAvailability.class.getSimpleName()));
            t.commit();
        }*/
    }

    @Override
    public void loadContactHost(String startDate, String endDate) {
        bundle = new Bundle();
        bundle.putString("checkInDate", startDate);
        bundle.putString("checkOutDate", endDate);
        FragmentContactHost fragmentContactHost = new FragmentContactHost();
        fragmentContactHost.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameSecondActivity, fragmentContactHost)
                .commit();
    }
}
