package com.example.vacation_manager.Details;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoDetails;
import com.example.vacation_manager.R;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterReview extends RecyclerView.Adapter<AdapterReview.HolderReview> {
    View view;
    Context context;
    List<PojoDetails.Datum> dataReview;
    final String TAG="AdapterReview";

    public AdapterReview(List<PojoDetails.Datum> data, Context context) {
        this.dataReview = data;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderReview onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reviews_detailpage,parent,false);
        return new HolderReview(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderReview holder, int position) {
        PojoDetails.Datum pos=dataReview.get(position);
        if(pos.getUser().getAvatar().isEmpty()){
            holder.textViewNoReview.setVisibility(View.VISIBLE);
        }else{
            holder.textViewNoReview.setVisibility(View.GONE);
            Picasso.with(context).load("http://vm.appening.xyz/" +dataReview.get(position).getUser().getAvatar() )
                    .fit().into(holder.imageViewReview);
        }
        Log.d(TAG, "onBindViewHolder: "+dataReview.get(position).getUser().getAvatar());
        holder.textViewName.setText(dataReview.get(position).getUser().getFName());
        if(dataReview.get(position).getUser().getAbout()!=null)
        holder.textViewDescription.setText(dataReview.get(position).getUser().getAbout());
        holder.textViewDate.setText(String.valueOf(dataReview.get(position).getUser().getMonth()));
        holder.ratingBar.setRating(Float.parseFloat(String.valueOf(dataReview.get(position).getTotal())));

    }

    @Override
    public int getItemCount() {
        return dataReview.size();
    }

    public class HolderReview extends RecyclerView.ViewHolder {
        ImageView imageViewReview;
        TextView textViewName, textViewDescription, textViewDate, textViewNoReview;
        RatingBar ratingBar;
        public HolderReview(@NonNull View itemView) {
            super(itemView);
            imageViewReview = itemView.findViewById(R.id.imageViewReview);
            textViewName = itemView.findViewById(R.id.textViewNameReview);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewDescription = itemView.findViewById(R.id.textViewDescriptionReview);
            ratingBar = itemView.findViewById(R.id.ratingBarReviews);
            textViewNoReview=itemView.findViewById(R.id.textViewNoReview);
        }
    }
}
