package com.example.vacation_manager.Details;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vacation_manager.R;

public class FragmentCancellationPolicy extends Fragment {
    View view;
    String policy, title;
    WebView webView;
    TextView textViewTitle;
    final String TAG="FragmentCancellationPolicy";
    ImageView closeButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cancellation_policy, container, false);
        policy = getArguments().getString("policy");
        title = getArguments().getString("title");
        webView = view.findViewById(R.id.webView);
        textViewTitle = view.findViewById(R.id.textViewPolicy);
        closeButton=view.findViewById(R.id.closeButton);
        if(title.equals("Cancellation Policy")){
            textViewTitle.setText("Cancellation Policy");
            webView.loadData(policy, "text/html", "UTF-8");
        }else{
            textViewTitle.setText("Things to keep in mind");
            webView.loadData(policy, "text/html", "UTF-8");
        }
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        return view;
    }
}
