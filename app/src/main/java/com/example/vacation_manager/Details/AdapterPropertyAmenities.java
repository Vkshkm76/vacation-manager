package com.example.vacation_manager.Details;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoDetails;
import com.example.vacation_manager.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

class AdapterPropertyAmenities extends RecyclerView.Adapter<AdapterPropertyAmenities.HolderAmenities>{
public List<PojoDetails.AmenitiesMaster> amenitiesMaster;
private Context context;
private View view;

    public AdapterPropertyAmenities(List<PojoDetails.AmenitiesMaster> amenitiesMaster, Context context) {
        this.amenitiesMaster=amenitiesMaster;
        this.context= context;
    }

    @NonNull
    @Override
    public HolderAmenities onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_amenities_detailpage,parent,false);
        return new HolderAmenities(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderAmenities holder, int position) {
        Transformation tr = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(5)
                .oval(false)
                .build();
        if (amenitiesMaster.get(position).getIcon().equals(null)) {
            holder.textViewNoAmenities.setVisibility(View.VISIBLE);
        } else {
            holder.textViewNoAmenities.setVisibility(View.GONE);
            Picasso.with(context).load("http://vm.appening.xyz/" + amenitiesMaster.get(position).getIcon())
                    .into(holder.imageView);

        }
    }

    @Override
    public int getItemCount() {
        return amenitiesMaster.size();
    }

    public class HolderAmenities extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewNoAmenities;
        public HolderAmenities(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.imageViewAmenities);
            textViewNoAmenities=itemView.findViewById(R.id.textViewNoAmenities);
        }
    }
}
