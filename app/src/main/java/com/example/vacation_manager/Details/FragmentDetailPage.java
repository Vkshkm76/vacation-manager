package com.example.vacation_manager.Details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.vacation_manager.Api;
import com.example.vacation_manager.Pojo.PojoDetails;
import com.example.vacation_manager.R;
import com.example.vacation_manager.RetrofitInstance;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("all")
public class FragmentDetailPage extends Fragment implements OnMapReadyCallback {
    private AppBarLayout appBarLayout;
    private ImageView imageViewProperty, arrowBackButton, imageViewHostedBy;
    private RecyclerView recyclerGalleryDetail, recyclerViewReview, recyclerPropertySubImage, recyclerViewAmenities;
    private TextView textViewPrivateRoom, textViewTitle, textViewCity, textViewGuest, textViewRooms, textViewBedRooms, textViewBath, textViewAbout, textViewPricePerNight, textViewPrice7PlusDays, textViewWeekendPrice, textViewPrice30PlusDays,
            textViewAddress, textViewCityCountryZip, textViewNearbyExplore, textViewCancellationStrict, textViewThingsRead, textViewPriceTotal, textViewRatingCount,
            textViewHostedBy, textViewNoSubImage, textViewNoReview, textViewNoAmenitiesImage;
    private RatingBar ratingBar;
    private View view;
    private Context context;
    Button buttonCheckAvailabilty;
    Api apiDetail;
    final String TAG = "FragmentDetailPage";
    private LinearLayoutManager linearLayoutManager;
    int propertyId;
    ImageView backButton;
    Toolbar toolbar;
    Boolean b;
    private GoogleMap mMap;
    private Double latitude, longitude;
    private String city, state, country, title, address;
    private PojoDetails.Details details;
    private FragmentDetailPageListener fragmentDetailPageListener;
    private Menu mMenuItem, favourite;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setHasOptionsMenu(true);
        mMenuItem = menu;
        backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        inflater.inflate(R.menu.menu_back_like_share, menu);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favouriteButton:
                if (!b) {
                    item.setIcon(R.drawable.ic_favorite_red_24dp);
                    b = true;
                } else {
                    item.setIcon(R.drawable.ic_favorite_border_white_24dp);
                    b = false;
                }
                break;
            case R.id.share:
                String url = "http://vm.appening.xyz/property-detail/" + propertyId;
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Let me recommend you this property");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(intent, "Share via"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_detail_page, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getContext()).setSupportActionBar(toolbar);
        propertyId = getArguments().getInt("propertyId");
        connectApi(propertyId);
        findViews(view);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapViewDetail);
        mapFragment.getMapAsync(this);
        return view;
    }

    private void findViews(View view) {

        appBarLayout = view.findViewById(R.id.appBarLayout);
        imageViewProperty = view.findViewById(R.id.imageViewProperty);
        recyclerPropertySubImage = view.findViewById(R.id.recyclerPropertySubImage);
        textViewNoSubImage = view.findViewById(R.id.textViewNoImageSubImage);
        recyclerViewReview = view.findViewById(R.id.recyclerViewReview);
        textViewPrivateRoom = view.findViewById(R.id.textViewPrivateRoom);
        textViewTitle = view.findViewById(R.id.textViewTitle);
        textViewCity = view.findViewById(R.id.textViewCity);
        textViewGuest = view.findViewById(R.id.textViewGuest);
        textViewRooms = view.findViewById(R.id.textViewRooms);
        //  textViewRooms.setText(details.);

        textViewBedRooms = view.findViewById(R.id.textViewBedRooms);
        textViewBath = view.findViewById(R.id.textViewBath);
        textViewAbout = view.findViewById(R.id.textViewAbout);
        textViewPricePerNight = view.findViewById(R.id.textViewPricePerNight);
        textViewPrice7PlusDays = view.findViewById(R.id.textViewPrice7PlusDays);
        textViewWeekendPrice = view.findViewById(R.id.textViewWeekendPrice);
        textViewPrice30PlusDays = view.findViewById(R.id.textViewPrice30PlusDays);

        textViewAddress = view.findViewById(R.id.textViewAddress);
        textViewCityCountryZip = view.findViewById(R.id.textViewAddresssCity);
        textViewNearbyExplore = view.findViewById(R.id.textViewNearbyExplore);
        //textViewNearbyExplore.setText(details.);
        textViewCancellationStrict = view.findViewById(R.id.textViewCancellationStrict);
        textViewThingsRead = view.findViewById(R.id.textViewThingsRead);
        //textViewThingsRead.setText(details.get);
        imageViewHostedBy = view.findViewById(R.id.imageViewHostedBy);
        textViewHostedBy = view.findViewById(R.id.textViewHostedByName);
        textViewNoReview = view.findViewById(R.id.textViewNoReview);
        textViewNoAmenitiesImage = view.findViewById(R.id.textViewNoAmenitiesImage);
        textViewPriceTotal = view.findViewById(R.id.textViewPriceTotal);
        imageViewHostedBy = view.findViewById(R.id.imageViewHostedBy);
        ratingBar = view.findViewById(R.id.ratingBarDetail);
        textViewRatingCount = view.findViewById(R.id.textViewRatingCount);
        buttonCheckAvailabilty = view.findViewById(R.id.buttonCheckAvailabilty);
        buttonCheckAvailabilty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentDetailPageListener.loadBookingAvailability();
             //   getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity,new FragmentAvailability()).addToBackStack(null).commit();
            }
        });
    }

    private void connectApi(int propertyId) {
        Log.d(TAG, "connectApi: propertyId"+propertyId);
        String auth = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdmNWE1M2NlMWMzYjBjYmM1NGVlZWZiOGI0OGU2MzU4YjBmZjJkODlmNGJjOGIyY2Q4ZWY1ZmFkMmExYmYxZGM2NjY3MDgzZDJmYjAyMDAwIn0.eyJhdWQiOiIyIiwianRpIjoiN2Y1YTUzY2UxYzNiMGNiYzU0ZWVlZmI4YjQ4ZTYzNThiMGZmMmQ4OWY0YmM4YjJjZDhlZjVmYWQyYTFiZjFkYzY2NjcwODNkMmZiMDIwMDAiLCJpYXQiOjE1NjQ3MzkyODksIm5iZiI6MTU2NDczOTI4OSwiZXhwIjoxNTY3NDE3NzQ5LCJzdWIiOiI0NiIsInNjb3BlcyI6WyIqIl19.p33eSvY5VosWZ1tMtT3qbfF8S02BZS-dDMqW6TFyZO8FMkd1x17qZo5Qdfi2opnZzn0oCLdL2aiGl6YrPz8vggyhzzwKsuk9_SHC0433jAWfKJvhdwg-UL4-_AfldPv404Pv5aarfU-YDVwigxfdKyu0aoH33myQCPg8Xgu2WTD75sn5HYgOD40hdF4T1aaQNaKSeFJyVMn4Pk4bR-u8lZqo-SlFZ8iv0a0Rv1pN0evjmI4d9xgVCtKiTsHOL1hV-0AL1P9N0KJojVyjVnQDY2wvjFml_NzzJ5zRNkbRGKdgBPqm5Y2sXNEAOf4giMGBjNLwstynxwfeJd9mq9NOPmwCGojZjTsJ3b_uCjjHFYHL6kqA1pZheXif2CdUcCKlYvxfc_fgnzMpbRWrhLhTFsw9-xZyDORP-GN2NLCJYP9rxb6YfuwPh0O9mJpeDUitT8SopMU66LfRzQgv5r3AEa-Fp8CAIUL0oVaHTUDw_r9DSvZieg3jobXbJ81d-xl8dzzVlWJYtUFFCrgpquyGJAOiyA7NQVUjELXrgVYUbQn1a670qllJ_nVJyoSM3L0dsrvBPvvPTD55sesSqowug3r1SkXFCl8NTUFkC1MC--REBOm0ZT3r26BvRweKdL7KoBWtgQRwbEHjptm0MyangJZS1irK-XeGsIEyvTTum8s";
        apiDetail = RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoDetails> call = apiDetail.getDetailsCall(propertyId, auth);
        call.enqueue(new Callback<PojoDetails>() {
            @Override
            public void onResponse(Call<PojoDetails> call, Response<PojoDetails> response) {
                if (response.body() != null) {
                    feedData(response.body());
                }
            }

            private void amenities(List<PojoDetails.AmenitiesMaster> amenitiesMaster, List<Integer> amenties) {
                linearLayoutManager = new LinearLayoutManager(getContext());
                linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
                recyclerViewAmenities.setLayoutManager(linearLayoutManager);
                List<PojoDetails.AmenitiesMaster> object = new ArrayList<>();
                Log.d(TAG, "amenities: size" + amenitiesMaster.size() + "  " + amenties.size());

                for (int i = 0; i < amenties.size(); i++) {
                    for (int j = i; j < amenitiesMaster.size(); j++) {
                        if (amenitiesMaster.get(j).getId() == amenties.get(i)) {
                            object.add(amenitiesMaster.get(i));
                        }
                    }
                }
                if (amenitiesMaster.isEmpty()) {
                    textViewNoAmenitiesImage.setVisibility(View.VISIBLE);
                } else {
                    textViewNoAmenitiesImage.setVisibility(View.GONE);
                    AdapterPropertyAmenities adapterAmenities = new AdapterPropertyAmenities(object, getContext());
                    recyclerViewAmenities.setAdapter(adapterAmenities);
                    adapterAmenities.notifyDataSetChanged();
                }
            }

            private void feedData(PojoDetails property) {
                PojoDetails.Details detail = property.getDetails();
                List<Integer> amenties = property.getAmenities();
                PojoDetails.Review review = property.getReview();
                setFavouriteData(detail);
                details = detail;
                final PojoDetails.Owner owner = property.getOwner();

                final PojoDetails.CancellationPolicy policy = property.getCancellationPolicy();
                recyclerPropertySubImage = view.findViewById(R.id.recyclerPropertySubImage);
                recyclerViewAmenities = view.findViewById(R.id.recyclerViewAmenities);
                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerPropertySubImage.setLayoutManager(llm);

                if (detail.getPropertyImage() != null)
                    Picasso.with(getContext())
                            .load("http://vm.appening.xyz/" + detail.getPropertyImage())
                            .into(imageViewProperty);
                Log.d(TAG, "feedData: else" + owner.getAvatar());
                /*if(!owner.getAvatar().equals(""))
                Picasso.with(getContext()).load("http://vm.appening.xyz/" + owner.getAvatar()).into(imageViewHostedBy);
                */
                textViewPrivateRoom.setText("" + detail.getCategory());
                textViewTitle.setText(detail.getTitle());
                textViewCity.setText(detail.getCity());
                textViewGuest.setText("" + detail.getGuestNumber());
                textViewBedRooms.setText("" + detail.getBedrooms());
                textViewBath.setText("" + detail.getBathrooms());
                textViewAbout.setText(detail.getDescription());
                textViewPricePerNight.setText("₹ " + detail.getPricePerNight());
                textViewPrice7PlusDays.setText("₹ " + detail.getPriceMoreThan7Days());
                textViewWeekendPrice.setText("₹ " + detail.getWeekendPrice());
                textViewPrice30PlusDays.setText("₹ " + detail.getPriceMoreThan30Days());
                textViewAddress.setText(detail.getAddress1());
                textViewCityCountryZip.setText("City: " + detail.getCity() + "\nCountry: " + detail.getCountry() + "\nZip: " + detail.getZip());
                textViewHostedBy.setText(owner.getFName());

                ratingBar.setRating(detail.getRating());
                textViewRatingCount.setText("" + detail.getNoOfReview());
                textViewPriceTotal.setText("₹ " + detail.getPricePerNight() + " per night");
                if (detail.getSubImages().isEmpty()) {
                    textViewNoSubImage.setVisibility(View.VISIBLE);
                } else {
                    textViewNoSubImage.setVisibility(View.GONE);
                    AdapterPropertySubImages adapter = new AdapterPropertySubImages(detail.getSubImages(), getContext());
                    recyclerPropertySubImage.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                amenities(property.getAmenitiesMaster(), amenties);

                recyclerViewReview = view.findViewById(R.id.recyclerViewReview);
                LinearLayoutManager llm3 = new LinearLayoutManager(getContext());
                llm3.setOrientation(RecyclerView.HORIZONTAL);
                recyclerViewReview.setLayoutManager(llm3);
                if (review.getData().isEmpty()) {
                    textViewNoReview.setVisibility(View.VISIBLE);
                } else {
                    textViewNoReview.setVisibility(View.GONE);
                    AdapterReview adapterReview = new AdapterReview(review.getData(), getContext());
                    adapterReview.notifyDataSetChanged();
                    recyclerViewReview.setAdapter(adapterReview);
                }
                AddMarker(property);

                textViewCancellationStrict.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!policy.getExtraDetail().isEmpty())
                            fragmentDetailPageListener.cancellationPolicy("Cancellation Policy", policy.getExtraDetail());
                    }
                });
                textViewThingsRead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!policy.getHouseRules().isEmpty())
                            fragmentDetailPageListener.cancellationPolicy("Things to keep in mind", policy.getHouseRules());
                    }
                });

                imageViewHostedBy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onClick: " + owner.getId());
                        if (owner.getId() != null)
                            fragmentDetailPageListener.ownerHostClick(owner.getId());
                    }
                });
            }

            @Override
            public void onFailure(Call<PojoDetails> call, Throwable t) {
            }
        });
    }

    private void setFavouriteData(PojoDetails.Details detail) {

    }


    private void AddMarker(PojoDetails property) {
        PojoDetails.Details details = property.getDetails();

        latitude = details.getLatitude();
        longitude = details.getLongitude();
        city = details.getCity();
        state = details.getState();
        country = details.getCountry();
        title = details.getTitle();
        address = city + ", " + country + ", " + state;

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
        IconGenerator iconFactory = new IconGenerator(getContext());
        iconFactory.setTextAppearance(R.style.markerTextStyle);

        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconFactory.makeIcon("" + address + "\n" + title)))
                .position(new LatLng(latitude, longitude)).
                        anchor(0.5f, 1.0f);

        mMap.addMarker(markerOptions);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                fragmentDetailPageListener.addLatLng(latitude, longitude, title);
                return true;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        fragmentDetailPageListener = (FragmentDetailPageListener) context;
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public interface FragmentDetailPageListener {
        void addLatLng(Double latitude, Double longitude, String title);
        void cancellationPolicy(String title, String policy);
        void ownerHostClick(int id);
        void loadBookingAvailability();
    }
}