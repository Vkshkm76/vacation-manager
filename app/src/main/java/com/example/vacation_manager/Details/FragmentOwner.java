package com.example.vacation_manager.Details;


import android.os.Bundle;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.vacation_manager.Api;
import com.example.vacation_manager.Pojo.PojoOwnerDetails;
import com.example.vacation_manager.R;
import com.example.vacation_manager.RetrofitInstance;
import com.google.android.material.appbar.AppBarLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@SuppressWarnings("all")
public class FragmentOwner extends Fragment {
    private AppBarLayout appBarLayout;
    private ImageView imageViewOwner;
    private Toolbar toolbar;
    private ImageView backButton;
    private TextView textViewOwnerName;
    private AppCompatRatingBar ratingBarOwner;
    private TextView textViewRatingCount;
    private TextView textViewCity;
    private TextView textViewJoiningDate;
    private TextView textViewOwnerDescription;
    private TextView textViewlanguage;
    private RecyclerView recyclerViewReview;
    private RecyclerView recyclerViewProperty;

    private View view;
    private int id;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_owner, container, false);
        getActivity().setActionBar(toolbar);
        id=getArguments().getInt("id");
        findViews();
        getApi(id);
        return view;
    }

    private void getApi(int id) {
        Log.d("ownerId", "getApi: "+id);
        final Api api= RetrofitInstance.getRetrofitInstance().create(Api.class);
        Call<PojoOwnerDetails> call = api.getOwnerData(id);
        call.enqueue(new Callback<PojoOwnerDetails>() {
            @Override
            public void onResponse(Call<PojoOwnerDetails> call, Response<PojoOwnerDetails> response) {
                if(response.isSuccessful()&& response.body()!=null)
                ownerData(response.body());
            }
            @Override
            public void onFailure(Call<PojoOwnerDetails> call, Throwable t) {
            }
        });
    }

    private void ownerData(PojoOwnerDetails detail1) {
        PojoOwnerDetails.Detail detail = detail1.getDetail();
        PojoOwnerDetails.Reviews reviews = detail1.getReviews();
        List<PojoOwnerDetails.PropertyList> propertyLists = detail1.getPropertyList();

        textViewOwnerName.setText(detail.getFName());
        textViewJoiningDate.setText(detail.getCreatedAt());
        Picasso.with(getContext()).load("http://vm.appening.xyz/"+detail.getAvatar()).into(imageViewOwner);
        if(detail.getLanguage()!=null)
        textViewlanguage.setText(""+detail.getLanguage());
        if(detail.getAbout()!=null)
        textViewOwnerDescription.setText(detail.getAbout().toString());
        if(reviews.getAverageRating()!=null)
        ratingBarOwner.setRating(Float.parseFloat(String.valueOf(reviews.getAverageRating())));
        if(reviews.getTotalCount()!=null)
        textViewRatingCount.setText(""+reviews.getTotalCount());
        if(detail.getCity()!=null)
        textViewCity.setText(detail.getCity().toString());

        AdapterOwnerReview adapterOwnerReview = new AdapterOwnerReview(reviews.getData(), getContext());
        adapterOwnerReview.notifyDataSetChanged();
        recyclerViewReview.setAdapter(adapterOwnerReview);

        AdapterOwner adapter = new AdapterOwner(propertyLists,getContext());
        adapter.notifyDataSetChanged();
        recyclerViewProperty.setAdapter(adapter);
    }

    private void findViews() {
        appBarLayout = view.findViewById(R.id.appBarLayout);
        imageViewOwner = view.findViewById(R.id.imageViewOwner);
      //  toolbar = view.findViewById(R.id.toolbar);
        backButton = view.findViewById(R.id.backButton);
        textViewOwnerName = view.findViewById(R.id.textViewOwnerName);
        ratingBarOwner = view.findViewById(R.id.ratingBarOwner);
        textViewRatingCount = view.findViewById(R.id.textViewRatingCount);
        textViewCity = view.findViewById(R.id.textViewCity);
        textViewJoiningDate = view.findViewById(R.id.textViewJoiningDate);
        textViewOwnerDescription = view.findViewById(R.id.textViewOwnerDescription);
        textViewlanguage = view.findViewById(R.id.textViewlanguage);
        recyclerViewReview = view.findViewById(R.id.recyclerViewReview);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerViewReview.setLayoutManager(llm);

        recyclerViewProperty = view.findViewById(R.id.recyclerViewProperty);
        LinearLayoutManager llm2 = new LinearLayoutManager(getContext());
        llm2.setOrientation(RecyclerView.HORIZONTAL);
        recyclerViewProperty.setLayoutManager(llm2);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
    }
}
