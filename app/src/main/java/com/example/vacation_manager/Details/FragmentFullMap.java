package com.example.vacation_manager.Details;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vacation_manager.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

public class FragmentFullMap extends Fragment implements OnMapReadyCallback {
    View view;
    private Double latitude, longitude;
    private String title;
    GoogleMap mMap;
    String TAG = "fragmentFullMap";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_full_map, container, false);
        title = getArguments().getString("title");
        latitude = getArguments().getDouble("latitude");
        longitude = getArguments().getDouble("longitude");
        Log.d(TAG, "onCreateView: lat :" + latitude + " lon" + longitude + "title " + title);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        IconGenerator iconFactory = new IconGenerator(getContext());
        iconFactory.setTextAppearance(R.style.markerTextStyle);

 mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 15));
        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconFactory.makeIcon(title)))
                .position(new LatLng(latitude, longitude)).
                        anchor(0.5f, 1.0f);
        mMap.addMarker(markerOptions);
    }
}