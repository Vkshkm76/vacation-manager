package com.example.vacation_manager.Details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoOwnerDetails;
import com.example.vacation_manager.R;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterOwner extends RecyclerView.Adapter<AdapterOwner.HolderProperty> {
    List<PojoOwnerDetails.PropertyList> propertyLists;
    Context context;
    View view;
    String TAG="AdapterOwnerPropertyList";
    public AdapterOwner(List<PojoOwnerDetails.PropertyList> propertyLists, Context context) {
        this.propertyLists = propertyLists;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderProperty onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.row_owner_property,parent,false);
        return new HolderProperty(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderProperty holder, int position) {
        holder.textViewHouseName.setText(propertyLists.get(position).getName());
        holder.textViewHouseType.setText(propertyLists.get(position).getType());
        holder.textViewHousePrice.setText("₹ "+propertyLists.get(position).getPricePerNight());
//        holder.textViewRatingCount.setText(propertyLists.get(position).getRatingCount());
        holder.ratingBarOwner.setRating(propertyLists.get(position).getRatingCount());
        if(propertyLists.get(position).getImage()!=null) {
//            holder.textViewNoOwnerImage.setVisibility(View.GONE);
            Picasso.with(context).load("http://vm.appening.xyz/" + propertyLists.get(position).getImage())
                    .into(holder.imageViewOwnerProperty);
        }
        else
            holder.textViewNoOwnerImage.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return propertyLists.size();
    }

    public class HolderProperty extends RecyclerView.ViewHolder {
        ImageView imageViewOwnerProperty;
        TextView textViewHouseType, textViewHouseName,textViewHousePrice, textViewRatingCount, textViewNoOwnerImage;
        AppCompatRatingBar ratingBarOwner;
        public HolderProperty(@NonNull View itemView) {
            super(itemView);
            imageViewOwnerProperty =itemView.findViewById(R.id.imageViewOwnerProperty);
            textViewHouseName = itemView.findViewById(R.id.textViewHouseName);
            textViewHouseType = itemView.findViewById(R.id.textViewHouseType);
            textViewHousePrice =itemView.findViewById(R.id.textViewHousePrice);
            textViewRatingCount = itemView.findViewById(R.id.textViewRatingCount);
            ratingBarOwner =itemView.findViewById(R.id.ratingBarOwner);
            textViewNoOwnerImage = itemView.findViewById(R.id.textViewNoOwnerImage);
        }
    }
}
