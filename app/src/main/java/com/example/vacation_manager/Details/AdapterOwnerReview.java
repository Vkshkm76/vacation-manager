package com.example.vacation_manager.Details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vacation_manager.Pojo.PojoOwnerDetails;
import com.example.vacation_manager.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

class AdapterOwnerReview extends RecyclerView.Adapter<AdapterOwnerReview.HolderReview> {
    List<PojoOwnerDetails.Datum> reviews;
    final String TAG="AdapterOwnerReview";
    Context context;
    View view;

    public AdapterOwnerReview(List<PojoOwnerDetails.Datum> reviews, Context context) {
        this.reviews = reviews;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderReview onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.row_review_owner, parent, false);
        return new HolderReview(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderReview holder, int position) {
        if (reviews.get(position).getUser() != null) {
            if (reviews.get(position).getUser() != null)
                holder.textViewNameReview.setText(reviews.get(position).getUser().getFName());
                holder.textViewDate.setText(reviews.get(position).getUser().getCreatedAt());
        }
        holder.textViewDescriptionReview.setText(reviews.get(position).getReview());
        holder.ratingBarReviews.setRating(reviews.get(position).getRating());
        if(reviews.get(position).getUser().getAvatar()!=null)
        Picasso.with(context).load("http://vm.appening.xyz/"+reviews.get(position).getUser().getAvatar())
                .into(holder.imageViewReview);

    }
    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public class HolderReview extends RecyclerView.ViewHolder {
        CircularImageView imageViewReview;
        TextView textViewNameReview, textViewDate, textViewDescriptionReview;
        RatingBar ratingBarReviews;

        public HolderReview(@NonNull View itemView) {
            super(itemView);
            imageViewReview = itemView.findViewById(R.id.imageViewReview);
            textViewNameReview = itemView.findViewById(R.id.textViewNameReview);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewDescriptionReview = itemView.findViewById(R.id.textViewDescriptionReview);
            ratingBarReviews = itemView.findViewById(R.id.ratingBarReviews);
        }
    }
}
