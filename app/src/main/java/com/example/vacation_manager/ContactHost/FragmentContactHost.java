package com.example.vacation_manager.ContactHost;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vacation_manager.R;

@SuppressWarnings("all")
public class FragmentContactHost extends Fragment  {
    private Toolbar toolbar;
    private ImageView backButton;
    private TextView textViewCheckIn, textViewCheckOut,textViewGuest, textViewNote;
    private Button buttonSendMessage;
    EditText editTextMessageHost;
    View view;
    Context context;
    final String TAG="FragmentContactHost";
    String checkInDate,checkOutDate;
    public FragmentContactHost() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact_host,container,false);
        findViews();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        return view;
    }
    private void findViews() {
        toolbar = view.findViewById(R.id.toolbar);
        backButton = view.findViewById(R.id.backButton);
        textViewCheckIn = view.findViewById(R.id.textViewCheckIn);
        textViewCheckOut = view.findViewById(R.id.textViewCheckOut);
        textViewGuest = view.findViewById(R.id.textViewGuest);
        editTextMessageHost= view.findViewById(R.id.editTextMessageHost);
        textViewNote = view.findViewById(R.id.textViewNote);
        buttonSendMessage = view.findViewById(R.id.buttonSendMessage);

      /*  textViewCheckIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (getFragmentManager() != null)
                    getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity, new FragmentCalender()).commit();
            }
        });
        textViewCheckOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (getFragmentManager() != null)
                    getFragmentManager().beginTransaction().replace(R.id.frameSecondActivity, new FragmentCalender()).commit();
            }
        });
        */if (getArguments() != null) {
            textViewCheckIn.setText(getArguments().getString("checkInDate"));
            textViewCheckOut.setText(getArguments().getString("checkOutDate"));
        }
    }
}
